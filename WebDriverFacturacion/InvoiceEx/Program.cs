﻿using System;
using System.Collections;
using System.IO;
using WebDriverFacturacion;


namespace InvoiceEx
{
     public class Program
    {
        static void Main(string[] args)
        {
            //ejecutarViva();
            //ejecutarAeromar();
            //ejecutarVolaris();
            //ejecutarAM();
            ejecutarCanada();
        }

        public static void ejecutarAM()
        {
            string txtInfoPath = "C:\\PrubaAM_FROM_STORED_P\\txt informativo\\";

            if (Directory.Exists(txtInfoPath) != true)
            {
                // Try to create the directory.
                DirectoryInfo di = Directory.CreateDirectory(txtInfoPath);
            }
            StreamWriter swProcesoPage = File.AppendText(txtInfoPath + "ProcesoPagina" + DateTime.Now.ToString("yyyyMMdd") + ".txt");
            swProcesoPage.WriteLine("Proceso Iniciado..." + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
            swProcesoPage.Close();

            FacturacionIVA fAM = new FacturacionIVA();
            fAM.peticionFacAero();

            StreamWriter swProcesoPageF = File.AppendText(txtInfoPath +  "ProcesoPagina" + DateTime.Now.ToString("yyyyMMdd") + ".txt");
            swProcesoPageF.WriteLine("Proceso Terminado..." + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
            swProcesoPageF.Close();

            //Environment.Exit(0);
        }

         public static void ejecutarCanada()
        {
            string txtInfoPath = "C:\\AirCanada\\txt informativo\\";

            if (Directory.Exists(txtInfoPath) != true)
            {
                // Try to create the directory.
                DirectoryInfo di = Directory.CreateDirectory(txtInfoPath);
            }
            StreamWriter swProcesoPage = File.AppendText(txtInfoPath + "ProcesoPagina" + DateTime.Now.ToString("yyyyMMdd") + ".txt");
            swProcesoPage.WriteLine("Proceso Iniciado..." + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
            swProcesoPage.Close();

            FacturacionIVA fAM = new FacturacionIVA();
            fAM.invoiceCanada();

            StreamWriter swProcesoPageF = File.AppendText(txtInfoPath + "ProcesoPagina" + DateTime.Now.ToString("yyyyMMdd") + ".txt");
            swProcesoPageF.WriteLine("Proceso Terminado..." + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
            swProcesoPageF.Close();

            Environment.Exit(0);
        }

         public static void ejecutarVolaris()
         {
             string txtInfoPath = "C:\\AirVolaris\\txt informativo\\";

             if (Directory.Exists(txtInfoPath) != true)
             {
                 // Try to create the directory.
                 DirectoryInfo di = Directory.CreateDirectory(txtInfoPath);
             }
             StreamWriter swProcesoPage = File.AppendText(txtInfoPath + "ProcesoPagina" + DateTime.Now.ToString("yyyyMMdd") + ".txt");
             swProcesoPage.WriteLine("Proceso Iniciado..." + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
             swProcesoPage.Close();

             FacturacionIVA fAM = new FacturacionIVA();
             fAM.invoiceVolaris();

             StreamWriter swProcesoPageF = File.AppendText(txtInfoPath + "ProcesoPagina" + DateTime.Now.ToString("yyyyMMdd") + ".txt");
             swProcesoPageF.WriteLine("Proceso Terminado..." + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
             swProcesoPageF.Close();

             Environment.Exit(0);
         }


         public static void ejecutarAeromar()
         {
             string txtInfoPath = "C:\\AirAeromar\\txt informativo\\";

             if (Directory.Exists(txtInfoPath) != true)
             {
                 // Try to create the directory.
                 DirectoryInfo di = Directory.CreateDirectory(txtInfoPath);
             }
             StreamWriter swProcesoPage = File.AppendText(txtInfoPath + "ProcesoPagina" + DateTime.Now.ToString("yyyyMMdd") + ".txt");
             swProcesoPage.WriteLine("Proceso Iniciado..." + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
             swProcesoPage.Close();

             FacturacionIVA fAM = new FacturacionIVA();
             fAM.invoiceAeromar();

             StreamWriter swProcesoPageF = File.AppendText(txtInfoPath + "ProcesoPagina" + DateTime.Now.ToString("yyyyMMdd") + ".txt");
             swProcesoPageF.WriteLine("Proceso Terminado..." + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
             swProcesoPageF.Close();

             Environment.Exit(0);
         }

         public static void ejecutarViva()
         {
             string txtInfoPath = "C:\\AirViva\\txt informativo\\";

             if (Directory.Exists(txtInfoPath) != true)
             {
                 // Try to create the directory.
                 DirectoryInfo di = Directory.CreateDirectory(txtInfoPath);
             }
             StreamWriter swProcesoPage = File.AppendText(txtInfoPath + "ProcesoPagina" + DateTime.Now.ToString("yyyyMMdd") + ".txt");
             swProcesoPage.WriteLine("Proceso Iniciado..." + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
             swProcesoPage.Close();

             FacturacionIVA fAM = new FacturacionIVA();
             fAM.buscarFacturaExistenteVivaaerobus();

             StreamWriter swProcesoPageF = File.AppendText(txtInfoPath + "ProcesoPagina" + DateTime.Now.ToString("yyyyMMdd") + ".txt");
             swProcesoPageF.WriteLine("Proceso Terminado..." + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
             swProcesoPageF.Close();

             Environment.Exit(0);
         }
    }
}
