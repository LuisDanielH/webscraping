﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebDriverFacturacion
{
    class DatosPeticionAirAeromar
    {

        private string rfc;

        public string Rfc
        {
            get { return rfc; }
            set { rfc = value; }
        }

        private string razonSocial;

        public string RazonSocial
        {
            get { return razonSocial; }
            set { razonSocial = value; }
        }

        private string calle;

        public string Calle
        {
            get { return calle; }
            set { calle = value; }
        }

        private string nExterior;

        public string NExterior
        {
            get { return nExterior; }
            set { nExterior = value; }
        }


        private string numeroI;

        public string NumeroI
        {
            get { return numeroI; }
            set { numeroI = value; }
        }

        private string colonia;

        public string Colonia
        {
            get { return colonia; }
            set { colonia = value; }
        }

            

        private string delegacion;

        public string Delegacion
        {
            get { return delegacion; }
            set { delegacion = value; }
        }



        private DateTime reservationDate;

        public DateTime ReservationDate
        {
            get { return reservationDate; }
            set { reservationDate = value; }
        }

        private string estado;

        public string Estado
        {
            get { return estado; }
            set { estado = value; }
        }

        private string CodifoP;

        public string CodigoP
        {
            get { return CodifoP; }
            set { CodifoP = value; }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        private string pnr;

        public string Pnr
        {
            get { return pnr; }
            set { pnr = value; }
        }

        private string fechaE;

        public string FechaE
        {
            get { return fechaE; }
            set { fechaE = value; }
        }

        private string pais;

        public string Pais
        {
            get { return pais; }
            set { pais = value; }
        }
        

    }
}
