﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Opera;
using OpenQA.Selenium.Chrome;
using System.Collections.Generic;
using OpenQA.Selenium.PhantomJS;
using System.Threading;
using System.Drawing;
using System.Configuration;
using OpenQA.Selenium.Support.UI;
using System.Timers;
using OpenQA.Selenium.Interactions;
using System.IO;

using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;



namespace WebDriverFacturacion
{
	[TestClass]
	public class FacturacionIVA
	{
		string response = "";

		/// <summary>
		/// Metodo utilizado para tramitar factura, y en caso de que exista factura la descargue (usando el metodo de "buscarFacturaExistente")
		/// </summary>
		/// <param name="PNR"></param>
		/// <param name="FirstName"></param>
		/// <param name="LastName"></param>
		/// <param name="RFCGrupo"></param>
		/// <param name="DireccionGrupo"></param>
		/// <param name="Colonia"></param>
		/// <param name="CPGrupo"></param>
		/// <param name="NombreGrupo"></param>
		/// <param name="Municipio"></param>
		/// <param name="Estado"></param>
		/// <param name="Email"></param>
		[TestMethod]
		public void invoiceInterjet()
		{
			InfoLayOut ilo = new InfoLayOut();

			var options = new ChromeOptions();

			options.AddUserProfilePreference("download.default_directory", "C:\\Interjet\\XML_PDF");
			options.AddUserProfilePreference("download.prompt_for_download", false);
			options.AddUserProfilePreference("download.directory_upgrade", true);
			options.AddUserProfilePreference("safebrowsing.enabled", true);
			options.AddUserProfilePreference("disable-popup-blocking", "true");
			options.AddArgument("--disable-popup-blocking");

			string downloadPath = "C:\\Interjet\\XML_PDF";

			if (Directory.Exists(downloadPath) != true)
			{
				// Try to create the directory.
				DirectoryInfo di = Directory.CreateDirectory(downloadPath);
			}




			List<DatosPeticionInterjet> datosLay = ilo.getDataFromfn_invInterjet();

			var driver = new ChromeDriver(options);
			try
			{

				foreach (var item in datosLay)
				{
					string NoHayFactura = "";
					//NoHayFactura = buscarFacturaExistenteInterjet(item.Pnr, item.Rfc);
					NoHayFactura = buscarFacturaExistenteInterjet("B4D2WI", "NAZS641215644");


					if (NoHayFactura != "")
					{
						driver.Navigate().GoToUrl("https://factura.interjet.com");
						driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));

						var divName = item.NombreApellido.Split('/');

						IWebElement PNRfAC = driver.FindElement(By.Id("PNR"));
						PNRfAC.SendKeys("B4D2WI");
						//pnr.SendKeys(item.Pnr);

						IWebElement FName = driver.FindElement(By.Id("FirstName"));
						//FName.SendKeys(divName[1].Replace('.', ' '));
						FName.SendKeys("MIREYA");

						IWebElement LName = driver.FindElement(By.Id("LastName"));
						//LName.SendKeys(divName[0].Replace('.', ' '));
						LName.SendKeys("BLAKELY");

						IWebElement SBooking = driver.FindElement(By.Id("SearchBooking"));
						SBooking.Click();
						string ErrorFound = driver.FindElement(By.Id("ErrorDiv")).Text;

						try
						{
							if (ErrorFound != "")
							{
								string errorFounfPath = downloadPath.Substring(0, downloadPath.Length - 7) + "\\PNR_NO_ENCONTRADO\\";

								if (Directory.Exists(errorFounfPath) != true)
								{
									// Try to create the directory.
									DirectoryInfo di = Directory.CreateDirectory(errorFounfPath);
								}

								StreamWriter swProcesoPage = File.AppendText(errorFounfPath + "ProcesoPagina" + DateTime.Now.ToString("yyyyMMdd") + ".txt");

								swProcesoPage.WriteLine(DateTime.Now.ToString("yyyyMMdd HH:mm:ss") + " Ocurrio el sigueinte error: " + ErrorFound);
								swProcesoPage.Close();

							}

							else if (ErrorFound == "")
							{
								IWebElement RFCG = driver.FindElement(By.Id("RFCGrupo"));
								//RFCG.SendKeys(item.Rfc);
								RFCG.SendKeys("NAZS641215644");

								IWebElement DireccionG = driver.FindElement(By.Id("DireccionGrupo"));
								//DireccionG.SendKeys(item.Calle + ", numero exterior " + item.NExterior + ", numero interior " + item.NumeroI);
								DireccionG.SendKeys("BLVD LUIS DONALDO COLOSIO, numero exterior 2007, numero interior LOCAL 24");

								IWebElement Col = driver.FindElement(By.Id("Colonia"));
								Col.SendKeys("EX HACIENDA DE COSCOTITLAN");
								//Col.SendKeys(item.Colonia);

								IWebElement CPG = driver.FindElement(By.Id("CPGrupo"));
								//CPG.SendKeys(item.CodigoP.PadLeft(5, '0'));
								CPG.SendKeys("42080");


								//Via Web
								IWebElement LugarEmision = driver.FindElement(By.Id("LugarEmision"));
								var select = new SelectElement(LugarEmision);
								select.SelectByValue("1");

								IWebElement NombreG = driver.FindElement(By.Id("NombreGrupo"));
								//NombreG.SendKeys(item.RazonSocial);
								NombreG.SendKeys("SALVADOR NAVARRETE ZORRILLA");

								IWebElement Muni = driver.FindElement(By.Id("Municipio"));
								//Muni.SendKeys(item.Delegacion);
								Muni.SendKeys("PACHUCA");

								IWebElement Est = driver.FindElement(By.Id("Estado"));
								//Est.SendKeys(item.Estado);
								Est.SendKeys("HIDALGO");

								IWebElement Em = driver.FindElement(By.Id("Email"));
								Em.SendKeys("facturacioninterjet@gmail.com");

								IWebElement Button2 = driver.FindElement(By.Id("Button2"));
								Button2.Click();

								ErrorFound = driver.FindElement(By.Id("ErrorDiv")).Text;


								if (ErrorFound != "")
								{
									string errorFounfPath = downloadPath.Substring(0, downloadPath.Length - 7) + "\\PNR_NO_ENCONTRADO\\";

									if (Directory.Exists(errorFounfPath) != true)
									{
										// Try to create the directory.
										DirectoryInfo di = Directory.CreateDirectory(errorFounfPath);
									}

									StreamWriter swProcesoPage = File.AppendText(errorFounfPath + "ProcesoPagina" + DateTime.Now.ToString("yyyyMMdd") + ".txt");

									swProcesoPage.WriteLine(DateTime.Now.ToString("yyyyMMdd HH:mm:ss") + " Ocurrio el sigueinte error: " + ErrorFound);
									swProcesoPage.Close();

								}

								else
								{
									//Boton Facturar 
									IWebElement facturar = driver.FindElementById("Button1");
									facturar.Click();
									try
									{
										ErrorFound = driver.FindElementById("PrintMessage").Text;
										//regresar a nueva busqueda
									}
									catch (Exception)
									{
										buscarFacturaExistenteInterjet(item.Pnr, item.Rfc);
										throw;
									}
								}



							}
						}
						catch (Exception)
						{
							response = ErrorFound;

						}
					}

				}
			}
			catch (Exception e)
			{
				string errorPagePath = downloadPath.Substring(0, downloadPath.Length - 7) + "\\txt informativo\\";

				if (Directory.Exists(errorPagePath) != true)
				{
					// Try to create the directory.
					DirectoryInfo di = Directory.CreateDirectory(errorPagePath);
				}
				StreamWriter swProcesoPage = File.AppendText(errorPagePath + "ProcesoPagina" + DateTime.Now.ToString("yyyyMMdd") + ".txt");

				swProcesoPage.WriteLine("Fallo al acceder a algun elemento de la pagina " + e.Message);
				swProcesoPage.Close();
				driver.Quit();
			}
			driver.Quit();
		}

		[TestMethod]

		/// <summary>
		/// Metodo usado para buscar y descargar facturas
		/// </summary>
		/// <param name="PNR"></param>
		/// <param name="RFC"></param>
		/// <returns></returns>
		public string buscarFacturaExistenteInterjet(string PNR, string RFC)
		{
			InfoLayOut ilo = new InfoLayOut();
			var options = new ChromeOptions();

			options.AddUserProfilePreference("download.default_directory", "C:\\Interjet\\XML_PDF");
			options.AddUserProfilePreference("download.prompt_for_download", false);
			options.AddUserProfilePreference("download.directory_upgrade", true);
			options.AddUserProfilePreference("safebrowsing.enabled", true);
			options.AddUserProfilePreference("disable-popup-blocking", "true");
			options.AddArgument("--disable-popup-blocking");

			string downloadPath = "C:\\Interjet\\XML_PDF";

			if (Directory.Exists(downloadPath) != true)
			{
				// Try to create the directory.
				DirectoryInfo di = Directory.CreateDirectory(downloadPath);
			}
			//driver1.Manage().Window.Position = new Point(-1000, -2000);
			var driver = new ChromeDriver(options);
			//Buscar Facturas y/o notas existentes
			driver.Navigate().GoToUrl("https://factura.interjet.com/ReimpresionFactura.aspx");
			driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));



			IWebElement pnr = driver.FindElement(By.Id("PNR"));

			//pnr.SendKeys(item.Pnr);
			pnr.SendKeys("B4D2WI");

			IWebElement rfcB = driver.FindElement(By.Id("RFC"));

			//rfcB.SendKeys(item.Rfc);
			rfcB.SendKeys("NAZS641215644");

			driver.FindElementById("Buscar").Click();


			//Esperar a que cargue
			Thread.Sleep(3000);

			//Descarga todos los xml de fatura y de notas de factura

			//Descarga todos los pdf de factura y de notas de factura

			//Descargar el mas reciente
			try
			{
				IList<IWebElement> allPDF = driver.FindElements(By.LinkText("PDF"));
				int iPDF = 0;
				foreach (IWebElement element in allPDF)
				{
					allPDF[iPDF++].Click();

				}

				//Descarga todos los xml de fatura y de notas de factura
				IList<IWebElement> allXML = driver.FindElements(By.LinkText("XML"));
				int iXML = 0;
				foreach (IWebElement element in allXML)
				{
					allXML[iXML++].Click();
					Thread.Sleep(1500);
				}
				if (iXML != 0 || iPDF != 0)
				{
					driver.Quit();
					ilo.DescomprimirZIP(ilo.xml_pdf_to_zip(downloadPath));
					return "";

					//Proceso de zip_unzip_lecturaxml    
				}
				else
				{
					driver.Quit();
					return "No se encontraron facturas";

				}
			}
			catch (Exception)
			{

				driver.Quit();
				return "No se encontraron facturas";
			}


		}


		/// <summary>
		/// Metodo usado para pedir la factura o, en caso de que exista la factura, descargarla
		/// </summary>
		public void invoiceAlaska()
		{
			int saltarSigProceso = 0;
			InfoLayOut ilo = new InfoLayOut();

			var options = new ChromeOptions();
			options.AddUserProfilePreference("download.default_directory", "C:\\Alaska\\XML_PDF");
			options.AddUserProfilePreference("download.prompt_for_download", false);
			options.AddUserProfilePreference("download.directory_upgrade", true);
			options.AddUserProfilePreference("safebrowsing.enabled", true);
			options.AddUserProfilePreference("disable-popup-blocking", "true");
			options.AddArgument("--disable-popup-blocking");

			string downloadPath = "C:\\Alaska\\XML_PDF";

			if (Directory.Exists(downloadPath) != true)
			{
				// Try to create the directory.
				DirectoryInfo di = Directory.CreateDirectory(downloadPath);
			}

			List<DatosPeticionAlaka> datosLay = ilo.getDataFromfn_invAlaska();

			var driver = new ChromeDriver(options);
			driver.Manage().Window.Position = new Point(-2000, -3000);

			try
			{
				driver.Navigate().GoToUrl("https://facturacion.alaskaair.com/airline/AS");
				driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));
				foreach (var item in datosLay)
				{
					////Acceso con cuenta de usuario
					IWebElement ticket = driver.FindElement(By.Id("booking"));
					string cleanPNR = item.Pnr.Replace("EMD", "").Replace("E", "").Replace("A", "").Replace("\"", "").Insert(0, "027");
					ticket.SendKeys(cleanPNR);
					var fechaReserva = item.ReservationDate.ToString();
					var Reserva = fechaReserva.Split('/');
					string dia = Reserva[0];
					string mes = Reserva[1];
					string anio = Reserva[2].Substring(0, 4);

					IWebElement fecha = driver.FindElementById("date");
					fecha.Click();
					fecha.SendKeys(dia + mes + anio);

					IWebElement continuar = driver.FindElementByXPath("/html/body/main/div/section[3]/form/button");
					continuar.SendKeys(Keys.Enter);
					//si existe ya la factura
					try
					{
						IWebElement buscarpdf = driver.FindElement(By.XPath("//*[@id=\"ticketList\"]/tbody/tr/td[8]/a[1]"));
						buscarpdf.Click();

						IWebElement buscarxml = driver.FindElement(By.XPath("//*[@id=\"ticketList\"]/tbody/tr/td[8]/a[2]"));
						buscarxml.Click();

						Thread.Sleep(3000);
						saltarSigProceso = 1;


						ilo.DescomprimirZIP(ilo.xml_pdf_to_zip(downloadPath));

						IWebElement nuevaBusqueda = driver.FindElementByXPath("//*[@id=\"billingTicketsInformation\"]/div[2]/a");
						nuevaBusqueda.Click();

					}
					catch (Exception)
					{
						IWebElement rfc = driver.FindElementById("rfcToSearch");
						rfc.SendKeys(item.Rfc);


						IWebElement searchRFC = driver.FindElementById("searchRFC");
						searchRFC.Click();

						IWebElement rfc2 = driver.FindElementById("rfc");
						rfc2.Clear();
						rfc2.SendKeys(item.Rfc);

						IWebElement conf_rfc2 = driver.FindElementById("confirmationRFC");
						conf_rfc2.Clear();
						conf_rfc2.SendKeys(item.Rfc);

						IWebElement razonS = driver.FindElementById("name");
						razonS.Clear();
						razonS.SendKeys(item.RazonSocial);

						IWebElement calle = driver.FindElementById("street");
						calle.Clear();
						calle.SendKeys(item.Calle);

						if (item.NExterior == "")
							item.NExterior = ".";

						IWebElement exterior = driver.FindElementById("extNumber");
						exterior.Clear();
						exterior.SendKeys(item.NExterior);

						IWebElement interior = driver.FindElementById("intNumber");
						interior.Clear();
						interior.SendKeys(item.NumeroI);

						IWebElement colonia = driver.FindElementById("neighbourhood");
						colonia.Clear();
						colonia.SendKeys(item.Colonia);

						IWebElement ciudadDelegacion = driver.FindElementById("municipality");
						ciudadDelegacion.Clear();
						ciudadDelegacion.SendKeys(item.Delegacion);

						IWebElement ciudad = driver.FindElementById("city");
						ciudad.Clear();
						ciudad.SendKeys(item.Ciudad);

						IWebElement estado = driver.FindElementById("state");
						estado.Clear();
						estado.SendKeys(item.Estado);

						IWebElement pais = driver.FindElementById("country");
						pais.Clear();
						pais.SendKeys(item.Estado);

						IWebElement cp = driver.FindElementById("zipCode");
						cp.Clear();
						cp.SendKeys(item.CodigoP.PadLeft(5, '0'));

						IWebElement email = driver.FindElementByXPath("//*[@id=\"billingInformation\"]/div[2]/div[2]/div[7]/div[1]/label");
						email.Click();

						IWebElement descargar = driver.FindElementByXPath("//*[@id=\"billingInformation\"]/div[2]/div[2]/div[7]/div[2]/label");
						descargar.Click();

						IWebElement seleccionarTicket = driver.FindElementByXPath("//*[@id=\"ticketList\"]/tbody/tr/td[9]/div/label");
						seleccionarTicket.Click();

						IWebElement addEmail = driver.FindElementById("email");
						addEmail.Clear();
						addEmail.SendKeys("luis@mother.travel");

						IWebElement confirmationEmail = driver.FindElementById("confirmationEmail");
						confirmationEmail.Clear();
						confirmationEmail.SendKeys("luis@mother.travel");

						IWebElement generarFactura = driver.FindElementById("billingSubmit");
						generarFactura.Click();

						Thread.Sleep(5000);

						IWebElement pdfdown = driver.FindElementByXPath("/html/body/main/div/table/tbody/tr[2]/td[2]/a/img");
						pdfdown.Click();

						IWebElement xmldown = driver.FindElementByXPath("/html/body/main/div/table/tbody/tr[2]/td[3]/a/img");
						xmldown.Click();

						Thread.Sleep(3000);

						ilo.DescomprimirZIP(ilo.xml_pdf_to_zip(downloadPath));
					}









				}
				driver.Quit();
			}

			catch (Exception e)
			{

				string errorPagePath = downloadPath.Substring(0, downloadPath.Length - 7) + "\\txt informativo\\";

				if (Directory.Exists(errorPagePath) != true)
				{
					// Try to create the directory.
					DirectoryInfo di = Directory.CreateDirectory(errorPagePath);
				}

				StreamWriter swProcesoPage = File.AppendText(errorPagePath + "ProcesoPagina" + DateTime.Now.ToString("yyyyMMdd") + ".txt");

				swProcesoPage.WriteLine("Fallo al acceder a algun elemento de la pagina " + e.Message);
				swProcesoPage.Close();
				driver.Quit();
			}
		}


		public string buscarFacturaExistenteAlaska(string datePnr, string ticket)
		{

			var options = new ChromeOptions();
			options.AddUserProfilePreference("download.default_directory", "C:\\pruebaDownload");
			options.AddUserProfilePreference("download.prompt_for_download", false);
			options.AddUserProfilePreference("download.directory_upgrade", true);
			options.AddUserProfilePreference("safebrowsing.enabled", true);

			var driver1 = new ChromeDriver(options);
			//driver1.Manage().Window.Position = new Point(-1000, -2000);

			//Validar que no vengan valores vacios
			if (datePnr != "" && ticket.StartsWith("027") == true)
			{
				//Buscar Facturas y/o notas existentes
				// debe lucir el link como este https://facturacion.alaskaair.com/information/AS/18-08-2016/0279115391930
				driver1.Navigate().GoToUrl(Config.AlaskaPageToDownloadInvoice + datePnr + "/" + ticket);

				//Esperar a que cargue
				Thread.Sleep(3000);


				IList<IWebElement> boletoNoExisten = driver1.FindElements(By.Id("booking"));


				if (boletoNoExisten.Count != 0)
				{
					driver1.Quit();
					return "El Número de boleto o Reserva no existen, favor de verificar.";
				}
				else
				{
					IList<IWebElement> boletoNoTieneFactura = driver1.FindElements(By.Id("rfcToSearch"));
					if (boletoNoTieneFactura.Count == 0)
					{
						IWebElement buscarpdf = driver1.FindElement(By.XPath("//*[@id=\"ticketList\"]/tbody/tr/td[8]/a[1]"));
						buscarpdf.Click();

						IWebElement buscarxml = driver1.FindElement(By.XPath("//*[@id=\"ticketList\"]/tbody/tr/td[8]/a[2]"));
						buscarxml.Click();
						Thread.Sleep(1500);
						driver1.Quit();
						return "Factura descargada";
					}
					else
					{
						driver1.Quit();
						return "No existe ninguna factura para el ticket: " + ticket;
					}



				}
			}
			else
			{
				driver1.Quit();
				return "No se aceptan campos vacios o el ticket no empezo con 027";

			}



		}
        public static string cleanPNR;
		int nuevoProceso = 0;
		/// <summary>
		/// Metodo de Peticion de Facturas Aeromexico
		/// </summary>
		public void peticionFacAero()
		{

			InfoLayOut ilo = new InfoLayOut();
			ilo.DescomprimirZIP(@"C:\PrubaAM_FROM_STORED_P");
			var options = new ChromeOptions();
			options.AddUserProfilePreference("download.default_directory", "C:\\PrubaAM_FROM_STORED_P");
			options.AddUserProfilePreference("download.prompt_for_download", false);
			options.AddUserProfilePreference("download.directory_upgrade", true);
			options.AddUserProfilePreference("safebrowsing.enabled", true);
			options.AddUserProfilePreference("disable-popup-blocking", "true");
			options.AddArgument("--disable-popup-blocking");

			string downloadPath = @"C:\PrubaAM_FROM_STORED_P";

			if (Directory.Exists(downloadPath) != true)
			{
				// Try to create the directory.
				DirectoryInfo di = Directory.CreateDirectory(downloadPath);
			}




			List<DatosFacturaAeromexico> datosLay = ilo.getDataFromfn_inv();

			var driver = new ChromeDriver(options);
            //driver.Manage().Window.Position = new Point(-2000, -3000);
			try
			{
				driver.Navigate().GoToUrl("https://facturacion.aeromexico.com/Login.aspx?");
				driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));

				////Acceso con cuenta de usuario
				IWebElement Usuario = driver.FindElement(By.Id("email"));
				Usuario.SendKeys("carlosalberto@mother.travel");
				IWebElement password = driver.FindElement(By.Id("password"));
				password.SendKeys("Carlos1393");

				string txtInfoPath = "C:\\PrubaAM_FROM_STORED_P\\txt informativo\\";

				StreamWriter swProcesoPage = File.AppendText(txtInfoPath + "ProcesoPagina" + DateTime.Now.ToString("yyyyMMdd") + ".txt");
				swProcesoPage.WriteLine("Ingreso correctamente a la cuenta");
				swProcesoPage.Close();


				IWebElement ingresar = driver.FindElement(By.Id("btnIngresar"));
				try
				{
					ingresar.Click();
				}
				catch (Exception)
				{

					// throw new Exception(ex.Message);
				}

				
					foreach (var item in datosLay)
					{
						string[] componentesFecha = item.FechaE.ToString().Split('/');
						

						//09/11/2016 06:52:00 p. m.
						int mesActual = Convert.ToInt32(DateTime.Now.Month.ToString("d2"));
						int diaActual = Convert.ToInt32(DateTime.Now.Day.ToString("d2"));


						if ((diaActual < 10 && (mesActual == Convert.ToInt32(componentesFecha[1]) || (mesActual - 1) == Convert.ToInt32(componentesFecha[1]))) || diaActual > 10 && mesActual == Convert.ToInt32(componentesFecha[1]))
						{


							if (nuevoProceso != 0)
							{
								IWebElement nuevosBoletos = driver.FindElementByXPath("//*[@id=\"ainic\"]");
								nuevosBoletos.Click();
								nuevoProceso = 0;
							}


							IWebElement boleto = driver.FindElementByXPath("//*[@id=\"MainContent_ctlBoletos_noBoletoEMD\"]");
							//string cleanPNR = item.Pnr.Replace("EMD", "").Replace("E", "").Replace("A", "");
							cleanPNR = item.Pnr.Replace("EMD", "").Replace("E", "").Replace("A", "").Replace("\"", "");
							boleto.SendKeys(cleanPNR + ",");

							try
							{

								IWebElement addRecibo = driver.FindElementByXPath("//*[@id=\"MainContent_ctlBoletos_btnAgregar\"]");
								addRecibo.Click();
								//justOne++;
								Actions actions = new Actions(driver);
								actions.SendKeys(Keys.Escape);

								Thread.Sleep(2000);
								if (driver.SwitchTo().Alert().Text != null)
								{
									IAlert alert = driver.SwitchTo().Alert();
									string alertaaa = alert.Text;
									alert.Dismiss(); // alternatively alert.dismiss();

									if (alertaaa.Contains("Este boleto fue facturado previamente") == true)
									{
										//bajar la factura
										driver.Navigate().GoToUrl("https://facturacion.aeromexico.com/MisFacturas.aspx");
										IWebElement buscarboleto = driver.FindElementById("MainContent_noBoleto");
										buscarboleto.SendKeys(cleanPNR);

										string dia = DateTime.Now.Day.ToString("d2");
										string mes = DateTime.Now.Month.ToString("d2");
										string anio = DateTime.Now.Year.ToString();
										int mesInt = Convert.ToInt32(mes) - 1;
										mes = mesInt.ToString();
										string fecha = dia + "/" + mes + "/" + anio;

										IWebElement fechaTicket = driver.FindElementById("ctl00_MainContent_fechaIni_dateInput");
										fechaTicket.SendKeys(fecha);

										IWebElement fechaHasta = driver.FindElementById("ctl00_MainContent_fechaA_dateInput");
										fechaHasta.SendKeys(dia + "/" + DateTime.Now.Month.ToString("d2") + "/" + anio);

										IWebElement buscar = driver.FindElementByXPath("//*[@id=\"MainContent_btnBuscar\"]");
										buscar.Click();
										try
										{
											if (driver.SwitchTo().Alert().Text != null)
											{
												IAlert alert2 = driver.SwitchTo().Alert();
												alert.Dismiss(); // alternatively alert.dismiss();
											}
										}
										catch (Exception)
										{


										}

										try
										{
											IWebElement seleccionrTicket = driver.FindElementById("ctl00_MainContent_gridResult_ctl00_ctl04_columnSelectCheckBox");
											seleccionrTicket.Click();
											//esperar mas por el tiempo de AM
											Thread.Sleep(3000);
											IWebElement descargar = driver.FindElementById("MainContent_btnDescargar");
											descargar.Click();
											Thread.Sleep(10000);
											bool finishInsertUpdate = false;
											while (finishInsertUpdate == false)
											{
												finishInsertUpdate = ilo.DescomprimirZIP(downloadPath);
											}
										}
										catch (Exception)
										{


										}







									}
									else if (alertaaa.Contains("cancela") == true)
									{
										int status = 44;
										ilo.cambioStatus(status, cleanPNR);
									}

									else if (alertaaa.Contains("ningun cargo adicional"))
									{
										//cargo sin cambio adicional
										int status = 39;
										ilo.cambioStatus(status, cleanPNR);
									}
									else if (alertaaa.Contains("no es suceptible"))
									{
										//No es suceptible de facturación debido a: Realizó un Canje al boleto emitido originalmente este puede facturarlo en un lapso de 48 horas hábiles posteriores a su fecha de emisión.
										int status = 46;
										ilo.cambioStatus(status, cleanPNR);
										//crear metodo para cambiar status
									}


								}
							}
							catch (Exception)
                            {


							}


							try
							{
								IWebElement siguiente = driver.FindElementByXPath("//*[@id=\"MainContent_ctlBoletos_btnContinuar\"]");
								siguiente.Click();

								IWebElement rfc = driver.FindElementById("ctl00_MainContent_ctlDatosFis_rfcAsig_Input");
								rfc.Clear();
								rfc.SendKeys(item.Rfc);

								IWebElement razonS = driver.FindElementById("MainContent_ctlDatosFis_razonSocial");
								razonS.Click();
								Thread.Sleep(5000);
								IWebElement razonS2 = driver.FindElementById("MainContent_ctlDatosFis_razonSocial");
								razonS2.Clear();
								razonS2.SendKeys(item.RazonSocial);

								IWebElement calle = driver.FindElementById("MainContent_ctlDatosFis_calle");
								calle.Clear();
								calle.SendKeys(item.Calle);

								if (item.NExterior == "")
									item.NExterior = ".";

								IWebElement exterior = driver.FindElementById("MainContent_ctlDatosFis_noExt");
								exterior.Clear();
								exterior.SendKeys(item.NExterior);

								IWebElement interior = driver.FindElementById("MainContent_ctlDatosFis_noInt");
								interior.Clear();
								interior.SendKeys(item.NumeroI);

								IWebElement colonia = driver.FindElementById("MainContent_ctlDatosFis_colonia");
								colonia.Clear();
								colonia.SendKeys(item.Colonia);

								IWebElement ciudadDelegacion = driver.FindElementById("MainContent_ctlDatosFis_poblacion");
								ciudadDelegacion.Clear();
								ciudadDelegacion.SendKeys(item.Delegacion);

								IWebElement mail = driver.FindElementById("MainContent_ctlDatosFis_email");
								mail.Clear();
								mail.SendKeys("luis@mother.travel");


								IWebElement estado = driver.FindElementById("MainContent_ctlDatosFis_comboEstado");
								string Est = ilo.estado(item.Estado);
								var select = new SelectElement(estado);
								select.SelectByValue(Est);

								IWebElement cp = driver.FindElementById("MainContent_ctlDatosFis_cp");
								cp.Clear();
								cp.SendKeys(item.CodigoP.PadLeft(5, '0'));

								IWebElement siguiente2 = driver.FindElementById("MainContent_ctlDatosFis_btnContinuar");
								siguiente2.Click();
								Thread.Sleep(5000);
								try
								{

									driver.SwitchTo().Frame(driver.FindElementByXPath("//*[@id=\"RadWindowWrapper_ctl00_MainContent_wndMsg\"]/div[2]/iframe"));

									IWebElement confirmar = driver.FindElementById("btnSi");
									confirmar.Click();

									Actions actions = new Actions(driver);
									try
									{
										if (driver.SwitchTo().Alert().Text != null)
										{
											IAlert alert = driver.SwitchTo().Alert();
											string alertaaa = alert.Text;
											alert.Dismiss(); // alternatively alert.dismiss();
											

												

												if (alertaaa.Contains("Este boleto fue facturado previamente") == true)
												{
													//bajar la factura
													driver.Navigate().GoToUrl("https://facturacion.aeromexico.com/MisFacturas.aspx");
													IWebElement buscarboleto = driver.FindElementById("MainContent_noBoleto");
													buscarboleto.SendKeys(cleanPNR);

													string dia = DateTime.Now.Day.ToString("d2");
													string mes = DateTime.Now.Month.ToString("d2");
													string anio = DateTime.Now.Year.ToString();
													int mesInt = Convert.ToInt32(mes) - 1;
													mes = mesInt.ToString();
													string fecha = dia + "/" + mes + "/" + anio;

													IWebElement fechaTicket = driver.FindElementById("ctl00_MainContent_fechaIni_dateInput");
													fechaTicket.SendKeys(fecha);

													IWebElement fechaHasta = driver.FindElementById("ctl00_MainContent_fechaA_dateInput");
													fechaHasta.SendKeys(dia + "/" + DateTime.Now.Month.ToString("d2") + "/" + anio);

													IWebElement buscar = driver.FindElementByXPath("//*[@id=\"MainContent_btnBuscar\"]");
													buscar.Click();
													try
													{
														if (driver.SwitchTo().Alert().Text != null)
														{
															IAlert alert2 = driver.SwitchTo().Alert();
															alert.Dismiss(); // alternatively alert.dismiss();
														}
													}
													catch (Exception)
													{


													}

													try
													{
														IWebElement seleccionrTicket = driver.FindElementById("ctl00_MainContent_gridResult_ctl00_ctl04_columnSelectCheckBox");
														seleccionrTicket.Click();
														//esperar mas por el tiempo de AM
														Thread.Sleep(3000);
														IWebElement descargar = driver.FindElementById("MainContent_btnDescargar");
														descargar.Click();
														Thread.Sleep(10000);
														bool finishInsertUpdate = false;
														while (finishInsertUpdate == false)
														{
															finishInsertUpdate = ilo.DescomprimirZIP(downloadPath);
														}
													}
													catch (Exception)
													{


													}




												}


											
										}
									}
									catch (Exception)
									{


									}

									Thread.Sleep(2000);
									try
									{
										actions.SendKeys(Keys.Escape);
										try
										{
											if (driver.SwitchTo().Alert().Text != null)
											{
												IAlert alert = driver.SwitchTo().Alert();
												string alertaaa = alert.Text;
												alert.Dismiss(); // alternatively alert.dismiss();
												if (alertaaa.Contains("Este boleto fue facturado previamente") == true)
												{
													//bajar la factura
													driver.Navigate().GoToUrl("https://facturacion.aeromexico.com/MisFacturas.aspx");
													IWebElement buscarboleto = driver.FindElementById("MainContent_noBoleto");
													buscarboleto.SendKeys(cleanPNR);

													string dia = DateTime.Now.Day.ToString("d2");
													string mes = DateTime.Now.Month.ToString("d2");
													string anio = DateTime.Now.Year.ToString();
													int mesInt = Convert.ToInt32(mes) - 1;
													mes = mesInt.ToString();
													string fecha = dia + "/" + mes + "/" + anio;

													IWebElement fechaTicket = driver.FindElementById("ctl00_MainContent_fechaIni_dateInput");
													fechaTicket.SendKeys(fecha);

													IWebElement fechaHasta = driver.FindElementById("ctl00_MainContent_fechaA_dateInput");
													fechaHasta.SendKeys(dia + "/" + DateTime.Now.Month.ToString("d2") + "/" + anio);

													IWebElement buscar = driver.FindElementByXPath("//*[@id=\"MainContent_btnBuscar\"]");
													buscar.Click();
													try
													{
														if (driver.SwitchTo().Alert().Text != null)
														{
															IAlert alert2 = driver.SwitchTo().Alert();
															alert.Dismiss(); // alternatively alert.dismiss();
														}
													}
													catch (Exception)
													{


													}

													try
													{
														IWebElement seleccionrTicket = driver.FindElementById("ctl00_MainContent_gridResult_ctl00_ctl04_columnSelectCheckBox");
														seleccionrTicket.Click();
														//esperar mas por el tiempo de AM
														Thread.Sleep(3000);
														IWebElement descargar = driver.FindElementById("MainContent_btnDescargar");
														descargar.Click();
														Thread.Sleep(10000);
														bool finishInsertUpdate = false;
														while (finishInsertUpdate == false)
														{
															finishInsertUpdate = ilo.DescomprimirZIP(downloadPath);
														}
													}
													catch (Exception)
													{


													}




												}
											}
										}
										catch (Exception)
										{


										}

										driver.SwitchTo().Frame(driver.FindElementByXPath("//*[@id=\"RadWindowWrapper_ctl00_MainContent_wndMsg\"]/div[2]/iframe"));
										IWebElement actualizarInfo = driver.FindElementById("btnSi");

										actualizarInfo.Click();


										Thread.Sleep(2000);
										actions.SendKeys(Keys.Escape);
										try
										{
											if (driver.SwitchTo().Alert().Text != null)
											{
												IAlert alert = driver.SwitchTo().Alert();
												string alertaaa = alert.Text;
												alert.Dismiss(); // alternatively alert.dismiss();
												if (alertaaa.Contains("Este boleto fue facturado previamente") == true)
												{
													//bajar la factura
													driver.Navigate().GoToUrl("https://facturacion.aeromexico.com/MisFacturas.aspx");
													IWebElement buscarboleto = driver.FindElementById("MainContent_noBoleto");
													buscarboleto.SendKeys(cleanPNR);

													string dia = DateTime.Now.Day.ToString("d2");
													string mes = DateTime.Now.Month.ToString("d2");
													string anio = DateTime.Now.Year.ToString();
													int mesInt = Convert.ToInt32(mes) - 1;
													mes = mesInt.ToString();
													string fecha = dia + "/" + mes + "/" + anio;

													IWebElement fechaTicket = driver.FindElementById("ctl00_MainContent_fechaIni_dateInput");
													fechaTicket.SendKeys(fecha);

													IWebElement fechaHasta = driver.FindElementById("ctl00_MainContent_fechaA_dateInput");
													fechaHasta.SendKeys(dia + "/" + DateTime.Now.Month.ToString("d2") + "/" + anio);

													IWebElement buscar = driver.FindElementByXPath("//*[@id=\"MainContent_btnBuscar\"]");
													buscar.Click();
													try
													{
														if (driver.SwitchTo().Alert().Text != null)
														{
															IAlert alert2 = driver.SwitchTo().Alert();
															alert.Dismiss(); // alternatively alert.dismiss();
														}
													}
													catch (Exception)
													{


													}

													try
													{
														IWebElement seleccionrTicket = driver.FindElementById("ctl00_MainContent_gridResult_ctl00_ctl04_columnSelectCheckBox");
														seleccionrTicket.Click();
														//esperar mas por el tiempo de AM
														Thread.Sleep(3000);
														IWebElement descargar = driver.FindElementById("MainContent_btnDescargar");
														descargar.Click();
														Thread.Sleep(10000);
														bool finishInsertUpdate = false;
														while (finishInsertUpdate == false)
														{
															finishInsertUpdate = ilo.DescomprimirZIP(downloadPath);
														}
													}
													catch (Exception)
													{


													}




												}
											}
										}
										catch (Exception)
										{


										}

									}
									catch (Exception)
									{


									}


								}
								catch (Exception)
								{


								}






								//esperar mas por el tiempo de AM
								Thread.Sleep(3000);
								IWebElement descargar1 = driver.FindElementById("MainContent_ctlDescarga_btnDescarga");
								descargar1.Click();
								Thread.Sleep(10000);
								bool finishInsertUpdate2 = false;
								while (finishInsertUpdate2 == false)
								{
									finishInsertUpdate2 = ilo.DescomprimirZIP(downloadPath);
								}



							}
							catch (Exception)
							{


							}


							nuevoProceso = 1;
						}
					//}

					}

            }
	
			catch (Exception e)
			{
				
				string txtInfoPath = "C:\\PrubaAM_FROM_STORED_P\\txt informativo\\";

				if (Directory.Exists(txtInfoPath) != true)
				{
					// Try to create the directory.
					DirectoryInfo di = Directory.CreateDirectory(txtInfoPath);
				}

				StreamWriter swProcesoPage = File.AppendText(txtInfoPath + "ProcesoPagina" + DateTime.Now.ToString("yyyyMMdd") + ".txt");

				swProcesoPage.WriteLine("Fallo al acceder a algun elemento de la pagina " + e.Message);
				swProcesoPage.Close();
				driver.Quit();
			}



			driver.Quit();
		}


		/// <summary>
		/// Metodo para la busqueda y peticion de facturacion de vivaaerobus
		/// </summary>
		/// <returns></returns>
		public string buscarFacturaExistenteVivaaerobus()
		{

			InfoLayOut ilo = new InfoLayOut();

			var options = new ChromeOptions();
			options.AddUserProfilePreference("download.default_directory", "C:\\Vivaaerobus\\XML_PDF");
			options.AddUserProfilePreference("download.prompt_for_download", false);
			options.AddUserProfilePreference("download.directory_upgrade", true);
			options.AddUserProfilePreference("safebrowsing.enabled", true);

			string downloadPath = "C:\\Vivaaerobus\\XML_PDF";

			if (Directory.Exists(downloadPath) != true)
			{
				// Try to create the directory.
				DirectoryInfo di = Directory.CreateDirectory(downloadPath);
			}

            ilo.DescomprimirZIP("C:\\Vivaaerobus\\xml_pdf_zip");

			var driver1 = new ChromeDriver(options);
			//driver1.Manage().Window.Position = new Point(-1000, -2000);
			List<DatosPeticionVivaaerobues> datosLay = ilo.getDataFromfn_invVivaaerobus();

			
			try
			{

                //foreach (var item in datosLay)
                //{
					//Buscar Facturas y/o notas existentes
                    driver1.Navigate().GoToUrl("http://facturacion.vivaaerobus.com/");
					IWebElement rfc = driver1.FindElement(By.Id("RFC"));
                    //rfc.SendKeys(item.Rfc);
                    rfc.SendKeys("MET8908305M9");



					IWebElement clave = driver1.FindElement(By.Id("Clave"));
                    //clave.SendKeys(item.Pnr);
                    clave.SendKeys("Q8ELGG");

					IWebElement nombre = driver1.FindElement(By.Id("Nombre"));
                    //nombre.SendKeys(item.NombreApellido);
                    nombre.SendKeys("MAGDALENA");

					IWebElement ape = driver1.FindElement(By.Id("Ape"));
                    //ape.SendKeys(item.NombreApellido);
                    ape.SendKeys("RAMIREZ");

                    IWebElement buscar = driver1.FindElementById("btnFacturar");
					buscar.Click();
					string error = "";
					try
					{
						IWebElement datosOk = driver1.FindElementByXPath("/html/body/div/div[1]/form/div[2]/table[2]/tbody/tr/td[1]/div/table/tbody/tr/td[2]/div");
                        error = datosOk.Text + " en el ticket: " + "Q8ELGG";//poner item.pnr
						string logErrorPath = downloadPath.Substring(0, downloadPath.Length - 7) + "\\LogProcesados\\LogError\\";

						if (Directory.Exists(logErrorPath) != true)
						{
							// Try to create the directory.
							DirectoryInfo di = Directory.CreateDirectory(logErrorPath);
						}
						StreamWriter swLogError = File.AppendText(logErrorPath + "ErrorLog" + DateTime.Now.ToString("yyyyMMdd") + ".txt");
						swLogError.WriteLine(DateTime.Now.ToString("yyyyMMdd HH:mm:ss") + " Ocurrio el sigueinte error: " + error);

						return error;


					}
					catch (Exception)
					{

						IWebElement regimen = driver1.FindElementById("Regimen");
						regimen.Clear();
                        //regimen.SendKeys(item.RazonSocial);
                        regimen.SendKeys("EL MUNDO ES TUYO, S.A. DE C.V.");

						IWebElement calle = driver1.FindElementById("Calle");
						calle.Clear();
                        //calle.SendKeys(item.Calle);
                        calle.SendKeys("DIAGONAL SAN ANTONIO");

						IWebElement colonia = driver1.FindElementById("Colonia");
						colonia.Clear();
                        //colonia.SendKeys(item.Colonia);
                        colonia.SendKeys(" COL. NARVARTE");

						IWebElement localidad = driver1.FindElementById("Localidad");
						localidad.Clear();
                        //localidad.SendKeys(item.Delegacion);
                        localidad.SendKeys("BENITO JUAREZ");

						IWebElement estado = driver1.FindElementById("Estado");
						estado.Clear();
                        //estado.SendKeys(item.Estado);
                        estado.SendKeys("CIUDAD DE MÉXICO.");

						IWebElement pais = driver1.FindElementById("Pais");
						pais.Clear();
                        //pais.SendKeys(item.Pais);
                        pais.SendKeys("MEXICO");

						IWebElement codigoPostal = driver1.FindElementById("CodigoPostal");
						codigoPostal.Clear();
                        //codigoPostal.SendKeys(item.CodigoP);
                        codigoPostal.SendKeys("03020");

						IWebElement email = driver1.FindElementById("Email");
						email.Clear();
						email.SendKeys("luis@mother.travel");

						IWebElement emailOpc = driver1.FindElementById("EmailOpc");
						emailOpc.Clear();
						emailOpc.SendKeys("luis@mother.travel");

						IWebElement continuar = driver1.FindElementByXPath("/html/body/div/div[1]/form/div[2]/table[2]/tbody/tr/td[2]/input");
						continuar.Click();
						try
						{
                            IWebElement reenvioFactura = driver1.FindElementByXPath("//*[@id=\"CheckRef1\"]");
                            reenvioFactura.Click();

                           
                            

                            IWebElement aceptar = driver1.FindElementById("ButonModalOk");
                            aceptar.Click();

                            IWebElement facturar = driver1.FindElementByXPath("//*[@id=\"btnFacturar\"]");
                            facturar.Click();

                            IWebElement facturar2 = driver1.FindElementByXPath("//*[@id=\"continuar\"]");
                            facturar2.Click();

                            IWebElement pdf = driver1.FindElementByXPath("//*[@id=\"DivMain\"]/div[1]/form/div[2]/table[1]/tbody/tr/td/input[1]");
                            pdf.Click();

                            IWebElement xml = driver1.FindElementByXPath("//*[@id=\"DivMain\"]/div[1]/form/div[2]/table[1]/tbody/tr/td/input[2]");
                            xml.Click();

                            ilo.DescomprimirZIP(ilo.xml_pdf_to_zip(downloadPath));

                            IWebElement nuevoProceso = driver1.FindElementByXPath("//*[@id=\"DivMain\"]/div[1]/form/div[2]/table[2]/tbody/tr/td/input");
                            nuevoProceso.Click();


                            //IWebElement datosOk = driver1.FindElementByXPath("/html/body/div/div[1]/form/div[2]/table[2]/tbody/tr/td[1]/div/table/tbody/tr/td[2]/div");
                            //error = datosOk.Text;
							return error;
						}
						catch (Exception)
						{

							IWebElement itemEdo = driver1.FindElementByXPath("Check1");
							itemEdo.Click();

							//Aqui selecciona refacturar

							IWebElement re_facturar = driver1.FindElementByXPath("/html/body/div/div[1]/form/div[2]/table[2]/tbody/tr/td[2]/input");
							re_facturar.Click();
						}
					}
					finally
					{
						//IWebElement continuarFac = driver1.FindElementById("continuar");
						//continuarFac.Click();


					}
                //}
			}
			catch 
			{ 
			
			}
			return "something";
		}

		string errorNoTicket = "";
		string fechaCompleta = "";
		public void invoiceCanada()
		{
			int saltarSigProceso = 0;
			InfoLayOut ilo = new InfoLayOut();
			
			var options = new ChromeOptions();
			options.AddUserProfilePreference("download.default_directory", "C:\\AirCanada\\XML_PDF");
			options.AddUserProfilePreference("download.prompt_for_download", false);
			options.AddUserProfilePreference("download.directory_upgrade", true);
			options.AddUserProfilePreference("safebrowsing.enabled", true);
			options.AddUserProfilePreference("disable-popup-blocking", "true");
			options.AddArgument("--disable-popup-blocking");

			string downloadPath = "C:\\AirCanada\\XML_PDF";

			if (Directory.Exists(downloadPath) != true)
			{
				// Try to create the directory.
				DirectoryInfo di = Directory.CreateDirectory(downloadPath);
			}
            ilo.DescomprimirZIP(downloadPath);
			//ilo.DescomprimirZIP(ilo.xml_pdf_to_zip(downloadPath));
			List<DatosPeticionAirCanada> datosLay = ilo.getDataFromfn_invAirCanada();

			var driver = new ChromeDriver(options);
            //driver.Manage().Window.Position = new Point(-2000, -3000);
			try
			{
				
				foreach (var item in datosLay)
				{
					driver.Navigate().GoToUrl("https://aircanada.getyourinvoice.com/airline/AC");
					driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));
					////Acceso con cuenta de usuario
					IWebElement ticket = driver.FindElement(By.Id("booking"));
					string cleanPNR = item.Pnr.Replace("EMD", "").Replace("E", "").Replace("A", "").Replace("\"", "").Insert(0, "014");
					ticket.SendKeys(cleanPNR);
					var fechaReserva = item.ReservationDate.ToString();
					var Reserva = fechaReserva.Split('/');
					string dia = Reserva[0];
					string mes = Reserva[1];
					string anio = Reserva[2].Substring(0, 4);

					IWebElement fecha = driver.FindElementById("date");
					fecha.Click();
					fechaCompleta = dia + mes + anio;
					fecha.SendKeys(fechaCompleta);

					IWebElement continuar = driver.FindElementByXPath("/html/body/main/div/section[3]/form/button");
					continuar.SendKeys(Keys.Enter);

                    try
                    {
                        IWebElement reintentar = driver.FindElementByXPath("//*[@id=\"ticketList\"]/tbody/tr/td[9]/a");
                        reintentar.Click();
                    }
                    catch (Exception)
                    {
                        
                        
                    }
					//si existe ya la factura
					try
					{
						IWebElement buscarpdf = driver.FindElement(By.XPath("//*[@id=\"ticketList\"]/tbody/tr/td[8]/a[1]"));
						buscarpdf.Click();

						IWebElement buscarxml = driver.FindElement(By.XPath("//*[@id=\"ticketList\"]/tbody/tr/td[8]/a[2]"));
						buscarxml.Click();

						Thread.Sleep(3000);
						saltarSigProceso = 1;


						ilo.DescomprimirZIP(ilo.xml_pdf_to_zip(downloadPath));

						IWebElement nuevaBusqueda = driver.FindElementByXPath("//*[@id=\"billingTicketsInformation\"]/div[2]/a");
						nuevaBusqueda.Click();

					}
					catch (Exception e)
					{
						try 
						{	        
							IWebElement rfc = driver.FindElementById("rfcToSearch");
							rfc.SendKeys(item.Rfc);

						   

							IWebElement searchRFC = driver.FindElementById("searchRFC");
							searchRFC.Click();

							Thread.Sleep(3000);

							IWebElement rfc2 = driver.FindElementById("rfc");
							rfc2.Clear();
							rfc2.SendKeys(item.Rfc);

							IWebElement conf_rfc2 = driver.FindElementById("confirmationRFC");
							conf_rfc2.Clear();
							conf_rfc2.SendKeys(item.Rfc);

							IWebElement razonS = driver.FindElementById("name");
							razonS.Clear();
							razonS.SendKeys(item.RazonSocial);

							IWebElement calle = driver.FindElementById("street");
							calle.Clear();
							calle.SendKeys(item.Calle);

							if (item.NExterior == "")
								item.NExterior = ".";

							IWebElement exterior = driver.FindElementById("extNumber");
							exterior.Clear();
							exterior.SendKeys(item.NExterior);

							IWebElement interior = driver.FindElementById("intNumber");
							interior.Clear();
							interior.SendKeys(item.NumeroI);

							IWebElement colonia = driver.FindElementById("neighbourhood");
							colonia.Clear();
							colonia.SendKeys(item.Colonia);

							IWebElement ciudadDelegacion = driver.FindElementById("municipality");
							ciudadDelegacion.Clear();
							ciudadDelegacion.SendKeys(item.Delegacion);

							IWebElement ciudad = driver.FindElementById("city");
							ciudad.Clear();
							ciudad.SendKeys(item.Ciudad);

							IWebElement estado = driver.FindElementById("state");
							estado.Clear();
							estado.SendKeys(item.Estado);

							IWebElement pais = driver.FindElementById("country");
							pais.Clear();
							pais.SendKeys(item.Ciudad);

							IWebElement cp = driver.FindElementById("zipCode");
							cp.Clear();
							cp.SendKeys(item.CodigoP.PadLeft(5, '0'));

							IWebElement email = driver.FindElementByXPath("//*[@id=\"billingInformation\"]/div[2]/div[2]/div[7]/div[1]/label");
							email.Click();

							IWebElement descargar = driver.FindElementByXPath("//*[@id=\"billingInformation\"]/div[2]/div[2]/div[7]/div[2]/label");
							descargar.Click();

							IWebElement seleccionarTicket = driver.FindElementByXPath("//*[@id=\"ticketList\"]/tbody/tr/td[10]/div/label");
							seleccionarTicket.Click();

							IWebElement addEmail = driver.FindElementById("email");
							addEmail.Clear();
							addEmail.SendKeys("luis@mother.travel");

							IWebElement confirmationEmail = driver.FindElementById("confirmationEmail");
							confirmationEmail.Clear();
							confirmationEmail.SendKeys("luis@mother.travel");

							IWebElement generarFactura = driver.FindElementById("billingSubmit");
							generarFactura.Click();

                            IAlert alert = driver.SwitchTo().Alert();
                            alert.Accept();

							Thread.Sleep(5000);

							IWebElement pdfdown = driver.FindElementByXPath("/html/body/main/div/table/tbody/tr[2]/td[2]/a/img");
							pdfdown.Click();

							IWebElement xmldown = driver.FindElementByXPath("/html/body/main/div/table/tbody/tr[2]/td[3]/a/img");
							xmldown.Click();

							Thread.Sleep(3000);

							ilo.DescomprimirZIP(ilo.xml_pdf_to_zip(downloadPath));
						}
						catch (Exception)
						{
							 string errorPagePath = downloadPath.Substring(0, downloadPath.Length - 7) + "\\txt informativo\\";

							if (Directory.Exists(errorPagePath) != true)
							{
								// Try to create the directory.
								DirectoryInfo di = Directory.CreateDirectory(errorPagePath);
							}

							StreamWriter swProcesoPage = File.AppendText(errorPagePath + "ProcesoPagina" + DateTime.Now.ToString("yyyyMMdd") + ".txt");

							swProcesoPage.WriteLine("El Número de boleto : " + item.Pnr + " o la fecha: " + fechaCompleta + " es incorrecta");
							swProcesoPage.Close();
							
						}
							
					   
						
					}









				}
				driver.Quit();
			}

			catch (Exception e)
			{

				string errorPagePath = downloadPath.Substring(0, downloadPath.Length - 7) + "\\txt informativo\\";

				if (Directory.Exists(errorPagePath) != true)
				{
					// Try to create the directory.
					DirectoryInfo di = Directory.CreateDirectory(errorPagePath);
				}

				StreamWriter swProcesoPage = File.AppendText(errorPagePath + "ProcesoPagina" + DateTime.Now.ToString("yyyyMMdd") + ".txt");

				swProcesoPage.WriteLine("Fallo al acceder a algun elemento de la pagina " + e.Message);
				swProcesoPage.Close();
				driver.Quit();
			}
		}

        public void invoiceVolaris()
        {
            int saltarSigProceso = 0;
            InfoLayOut ilo = new InfoLayOut();

            var options = new ChromeOptions();
            options.AddUserProfilePreference("download.default_directory", "C:\\AirVolaris\\XML_PDF");
            options.AddUserProfilePreference("download.prompt_for_download", false);
            options.AddUserProfilePreference("download.directory_upgrade", true);
            options.AddUserProfilePreference("safebrowsing.enabled", true);
            options.AddUserProfilePreference("disable-popup-blocking", "true");
            options.AddArgument("--disable-popup-blocking");

            string downloadPath = "C:\\AirVolaris\\XML_PDF";

            if (Directory.Exists(downloadPath) != true)
            {
                // Try to create the directory.
                DirectoryInfo di = Directory.CreateDirectory(downloadPath);
            }
            //ilo.DescomprimirZIP(ilo.xml_pdf_to_zip(downloadPath));
            List<DatosPeticionAirCanada> datosLay = ilo.getDataFromfn_invAirCanada();

            var driver = new ChromeDriver(options);
            //driver.Manage().Window.Position = new Point(-2000, -3000);
            

                
                    driver.Navigate().GoToUrl("https://webportal.edicomgroup.com/customers/volaris/busqueda-cfdi-ticket-volaris.html");
                    driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));
                    ////Acceso con cuenta de usuario
                    IWebElement clavePNR = driver.FindElementByXPath("//*[@id=\"gridbox\"]/div[2]/table/tbody/tr[2]/td/input");
                    clavePNR.SendKeys("G758SM");

                    IWebElement boton_aceptar = driver.FindElementById("boton_aceptar");
                    boton_aceptar.Click();

                    //poner time

                    IWebElement seleccionarPNR = driver.FindElementByXPath("//*[@id=\"gridbox2\"]/div[2]/table/tbody/tr[2]/td[1]/img");
                    seleccionarPNR.Click();

                    IWebElement boton_generar = driver.FindElementById("boton_generar");
                    boton_generar.Click();

                    IWebElement RFC_ = driver.FindElementById("rfc_");
                    RFC_.SendKeys("CIE800912J23");

                    IWebElement nombre = driver.FindElementById("nombre");
                    nombre.Clear();
                    nombre.SendKeys("CENTRO DE INVESTIGACIONES Y ESTUDIOS SUPERIORES EN ANTROPOLOGIA SOCIAL");

                    IWebElement calle = driver.FindElementById("calle");
                    calle.Clear();
                    calle.SendKeys("JUAREZ");

                    IWebElement noex = driver.FindElementById("noext");
                    noex.Clear();
                    noex.SendKeys("#87");

                    IWebElement noint = driver.FindElementById("noint");
                    noint.Clear();
                    noint.SendKeys(".");

                    IWebElement localidad = driver.FindElementById("localidad");
                    localidad.SendKeys("");

                    IWebElement municipio = driver.FindElementById("municipio");
                    municipio.Clear();
                    municipio.SendKeys("TLALPAN");

                    IWebElement codigoPostal = driver.FindElementById("codigoPostal");
                    codigoPostal.Clear();
                    codigoPostal.SendKeys("14000");

                    IWebElement colonia = driver.FindElementById("colonia");
                    colonia.Clear();
                    colonia.SendKeys("TLALPAN");

                    IWebElement estado = driver.FindElementById("estado");
                    estado.Clear();
                    estado.SendKeys("CIUDAD DE MÉXICO");

                    IWebElement email = driver.FindElementById("email1");
                    email.Clear();
                    email.SendKeys("luis@mother.travel");

                    IWebElement siguiente = driver.FindElementById("btnGenerar");
                    siguiente.Click();

                    //esperar

                    IWebElement descargar = driver.FindElementByXPath("//*[@id=\"divEnlace\"]/table/tbody/tr[1]/td/p/button[1]");
                    descargar.Click();

                    //Se tiene que ingresar a correo para recuperarlas
                driver.Quit();
            
        }

        public void invoiceAeromar()
        {
            
            InfoLayOut ilo = new InfoLayOut();

            var options = new ChromeOptions();
            options.AddUserProfilePreference("download.default_directory", "C:\\AirAeromar\\XML_PDF");
            options.AddUserProfilePreference("download.prompt_for_download", false);
            options.AddUserProfilePreference("download.directory_upgrade", true);
            options.AddUserProfilePreference("safebrowsing.enabled", true);
            options.AddUserProfilePreference("disable-popup-blocking", "true");
            options.AddArgument("--disable-popup-blocking");

            string downloadPath = "C:\\AirAeromar\\XML_PDF";
            //ilo.DescomprimirZIP(downloadPath);
            if (Directory.Exists(downloadPath) != true)
            {
                // Try to create the directory.
                DirectoryInfo di = Directory.CreateDirectory(downloadPath);
            }
            //ilo.DescomprimirZIP(ilo.xml_pdf_to_zip(downloadPath));
            List<DatosPeticionAirAeromar> datosLay = ilo.getDataFromfn_invAirAeromar();

            var driver = new ChromeDriver(options);
            //driver.Manage().Window.Position = new Point(-2000, -3000);



            driver.Navigate().GoToUrl("http://aeromar.e-facturate.com/tickets/");
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));

            bool existeFctura = false;
            try
            {

                foreach (var item in datosLay)
                {
                    IWebElement txt_ticket = driver.FindElementById("txt_ticket");
                    string cleanPNR = item.Pnr.Replace("EMD", "").Replace("E", "").Replace("A", "").Replace("\"", "").Insert(0, "942");
                    //txt_ticket.SendKeys(cleanPNR);
                    txt_ticket.SendKeys("9422207084616");



                    IWebElement RFC = driver.FindElementById("txt_rfccliente");
                    //RFC.SendKeys(item.Rfc);
                    RFC.SendKeys("MET8908305M9");

                    IWebElement AddTicket = driver.FindElementById("btn_agregarticket");
                    AddTicket.Click();
                    //Agregar Tiempo
                    Thread.Sleep(3000);

                    try
                    {
                       
                        IWebElement descargarExistente = driver.FindElementById("btn_dxmlpet");
                        
                        IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                        js.ExecuteScript("arguments[0].click()", descargarExistente);

                        IWebElement cerrarVentana = driver.FindElementById("btn_modalnotificacion");

                        IJavaScriptExecutor js2 = (IJavaScriptExecutor)driver;
                        js2.ExecuteScript("arguments[0].click()", cerrarVentana);
                        
                        ilo.DescomprimirZIP(downloadPath);
                        existeFctura = true;
                    }
                    catch (Exception)
                    {
                        
                        throw;
                    }
                    
                    if(existeFctura == false)
                    {
                        IWebElement siguiente = driver.FindElementByXPath("//*[@id=\"btn_continuar\"]/a");
                        siguiente.Click();

                        IWebElement pais = driver.FindElementById("cbo_cpais");

                        string Pa = (ilo.paisAeromar(item.Pais));
                        var select = new SelectElement(pais);
                        select.SelectByValue(Pa);

                        if (Pa != "MX")
                        {
                            IWebElement estado = driver.FindElementById("txt_cestado");
                            estado.SendKeys(item.Estado);
                        }

                        else if (Pa == "MX")
                        {
                            IWebElement estado = driver.FindElementById("cbo_cestados");

                            string Est = (ilo.estadoAeromar(item.Estado));
                            var selectEdo = new SelectElement(estado);
                            selectEdo.SelectByValue(Est);
                        }

                        IWebElement razonSocial = driver.FindElementById("txt_crsocial");
                        razonSocial.Clear();
                        razonSocial.SendKeys(item.RazonSocial);

                        IWebElement cp = driver.FindElementById("txt_ccp");
                        cp.Clear();

                        cp.SendKeys(item.CodigoP.PadLeft(5, '0'));

                        IWebElement municipio = driver.FindElementById("txt_cmunucipio");
                        municipio.Clear();
                        municipio.SendKeys(item.Delegacion);

                        IWebElement txt_ccolonia = driver.FindElementById("txt_ccolonia");
                        txt_ccolonia.Clear();
                        txt_ccolonia.SendKeys(item.Colonia);

                        IWebElement txt_ccalle = driver.FindElementById("txt_ccalle");
                        txt_ccalle.Clear();
                        txt_ccalle.SendKeys(item.Calle);

                        if (item.NExterior == "")
                            item.NExterior = ".";

                        IWebElement txt_cnumext = driver.FindElementById("txt_cnumext");
                        txt_cnumext.Clear();
                        txt_cnumext.SendKeys(item.NExterior);

                        IWebElement txt_cnumint = driver.FindElementById("txt_cnumint");
                        txt_cnumint.Clear();
                        txt_cnumint.SendKeys(item.NExterior);

                        IWebElement txt_clocalidad = driver.FindElementById("txt_clocalidad");
                        txt_clocalidad.Clear();
                        txt_clocalidad.SendKeys(item.Delegacion);

                        IWebElement txt_ccorreo = driver.FindElementById("txt_ccorreo");
                        txt_ccorreo.Clear();
                        txt_ccorreo.SendKeys("luis201260daniel@gmail.com");

                        IWebElement siguiente2 = driver.FindElementByXPath("//*[@id=\"btn_continuar\"]/a");
                        siguiente2.Click();

                        IWebElement siguiente3 = driver.FindElementByXPath("//*[@id=\"btn_continuar\"]/a");
                        siguiente3.Click();

                        IWebElement btn_modalnotificacion = driver.FindElementById("btn_modalnotificacion");
                        btn_modalnotificacion.Click();

                        IWebElement btn_dxmlpdf = driver.FindElementById("btn_dxmlpdf");
                        btn_dxmlpdf.Click();

                        Thread.Sleep(5000);

                        ilo.DescomprimirZIP(downloadPath);
                    }
                   

                    //Agregar boton para nueva factura  
                    IWebElement nuevaFactura = driver.FindElementByXPath("//*[@id=\"wizard\"]/div[2]/ul/li[3]/a");
                    nuevaFactura.Click();
                }
                
            }
            catch(Exception)
            {

            }
        }
	}
}

