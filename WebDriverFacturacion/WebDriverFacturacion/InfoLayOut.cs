﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data.Linq;
using System.IO.Compression;
using System.Xml.Linq;
using ICSharpCode.SharpZipLib.Zip;
using System.Xml;
using System.Globalization;


namespace WebDriverFacturacion
{
    class InfoLayOut
    {
        fn_invoice_cfdi cfdi = new fn_invoice_cfdi();
        string zipProcesado = "";
        string name = "";

        /// <summary>
        /// Metodo para realizar pruebas por medio de lay_out de excel
        /// </summary>
        /// <returns></returns>
        //public List<DatosFacturaAeromexico> getDataFromL_O()
        //{
            
        //    List<DatosFacturaAeromexico> _dataFact = new List<DatosFacturaAeromexico>();
        //    StreamReader srLO = new StreamReader("C:\\Users\\pande\\OneDrive\\Documentos\\Visual Studio 2013\\Projects\\WebDriverFacturacion\\WebDriverFacturacion\\LayOutcsv\\AM_03_OCT.csv", true);
        //    srLO.ReadLine();
            
        //    string lectura;
        //    while ((lectura = srLO.ReadLine()) != null)
        //    {
           

        //    var _datos = lectura.Split(',');
        //    _dataFact.Add(new DatosFacturaAeromexico() { Rfc = _datos[0], RazonSocial = _datos[1], Calle = _datos[2], NExterior = _datos[3], NumeroI = _datos[4], Colonia = _datos[5],  Delegacion = _datos[6], Estado = _datos[7], CodigoP = _datos[8], Email = _datos[9], Pnr = _datos[10], FechaE = _datos[11] });

        //    }

        //    srLO.Close();
        //    return _dataFact;
        //}

        /// <summary>
        /// Metodo para adquirir todos los tickets en estatus 3 de aeromexico
        /// </summary>
        /// <returns></returns>
        public List<DatosFacturaAeromexico> getDataFromfn_inv()
        {
            var date = DateTime.Now.ToString("yyyy-MM-dd") + "%";
            fn_invoiceAMDataContext db = new fn_invoiceAMDataContext(); 
            
            List<DatosFacturaAeromexico> _dataFact = (from i in db.SolicitarFacturasAM() select new DatosFacturaAeromexico() {Rfc = i.REQRFC, RazonSocial = i.NOMBRE___RAZON_SOCIAL, Calle = i.CALLE, NExterior = i.NUMERO_EXTERIOR, NumeroI = i.NUMERO_INTERIOR, Colonia = i.COLONIA, Delegacion = i.DELEGACION, Estado = i.ESTADO, CodigoP = i.CODIGO_POSTAL, Pnr = i.NO_ETICKET__PNR, FechaE = i.FECHA_DE_EMISION }).ToList();            
            
            return _dataFact;                                      
        }


        /// <summary>
        /// Metodo para adquirir todos los tickets en estatus 3 de Alaska
        /// </summary>
        /// <returns></returns>
        public List<DatosPeticionAlaka> getDataFromfn_invAlaska()
        {
            fn_invoiceAMDataContext fn = new fn_invoiceAMDataContext();
            List<DatosPeticionAlaka> _dataFact = (from c in fn.fn_invoices where c.AIRLINE_CODE == "027" && c.ID_STATUS == 3
                                                  select new DatosPeticionAlaka() {ReservationDate = c.RESERVATION_DATE, Rfc = c.REQRFC, RazonSocial = c.REQISSUE_NAME, Calle = c.REQSTREET, NExterior = c.REQEXTERNAL_NO, NumeroI = c.REQINTERNAL_NO, Colonia = c.REQCOLONIA, Delegacion = c.REQMUNICIPIO, Estado = c.REQESTADO, CodigoP = c.REQZIPCODE, Pnr = c.ETICKET_PNR, Ciudad = c.REQCOUNTRY }).ToList();            
            
            return _dataFact;                                      
        }

        /// <summary>
        /// Metodo para adquirir todos los tickets en status de AirCanada
        /// </summary>
        /// <returns></returns>
        public List<DatosPeticionAirCanada> getDataFromfn_invAirCanada()
        {
            fn_invoiceAMDataContext fn = new fn_invoiceAMDataContext();
            List<DatosPeticionAirCanada> _dataFact = (from c in fn.fn_invoices
                                                  where c.AIRLINE_CODE == "014" && c.ID_STATUS == 3
                                                      select new DatosPeticionAirCanada() { ReservationDate = c.RESERVATION_DATE, Rfc = c.REQRFC, RazonSocial = c.REQISSUE_NAME, Calle = c.REQSTREET, NExterior = c.REQEXTERNAL_NO, NumeroI = c.REQINTERNAL_NO, Colonia = c.REQCOLONIA, Delegacion = c.REQMUNICIPIO, Estado = c.REQESTADO, CodigoP = c.REQZIPCODE, Pnr = c.ETICKET_PNR, Ciudad = c.REQCOUNTRY }).ToList();

            return _dataFact;
        }

        public List<DatosPeticionAirAeromar> getDataFromfn_invAirAeromar()
        {
            fn_invoiceAMDataContext fn = new fn_invoiceAMDataContext();
            List<DatosPeticionAirAeromar> _dataFact = (from c in fn.fn_invoices
                                                      where c.AIRLINE_CODE == "942" && c.ID_STATUS == 3
                                                       select new DatosPeticionAirAeromar() { ReservationDate = c.RESERVATION_DATE, Rfc = c.REQRFC, RazonSocial = c.REQISSUE_NAME, Calle = c.REQSTREET, NExterior = c.REQEXTERNAL_NO, NumeroI = c.REQINTERNAL_NO, Colonia = c.REQCOLONIA, Delegacion = c.REQMUNICIPIO, Estado = c.REQESTADO, CodigoP = c.REQZIPCODE, Pnr = c.ETICKET_PNR, Pais = c.REQCOUNTRY }).ToList();

            return _dataFact;
        }
        /// <summary>
        /// Metodo para adquirir todos los tickets en estatus 3 de Interjet
        /// </summary>
        /// <returns></returns>
        public List<DatosPeticionInterjet> getDataFromfn_invInterjet()
        {
            fn_invoiceAMDataContext fn = new fn_invoiceAMDataContext();
            List<DatosPeticionInterjet> _dataFact = (from c in fn.fn_invoices
                                                  where c.AIRLINE_CODE == "837" && c.ID_STATUS == 3
                                                     select new DatosPeticionInterjet() {NombreApellido = c.PASSENGER, ReservationDate = c.RESERVATION_DATE, Rfc = c.REQRFC, RazonSocial = c.REQISSUE_NAME, Calle = c.REQSTREET, NExterior = c.REQEXTERNAL_NO, NumeroI = c.REQINTERNAL_NO, Colonia = c.REQCOLONIA, Delegacion = c.REQMUNICIPIO, Estado = c.REQESTADO, CodigoP = c.REQZIPCODE, Pnr = c.ETICKET_PNR, Ciudad = c.REQCOUNTRY }).ToList();

            return _dataFact;
        }


        /// <summary>
        /// Metodo para adquirir todos los tickets en estatus 3 de Vivaaerobus
        /// </summary>
        /// <returns></returns>
        public List<DatosPeticionVivaaerobues> getDataFromfn_invVivaaerobus()
        {
            fn_invoiceAMDataContext fn = new fn_invoiceAMDataContext();
            List<DatosPeticionVivaaerobues> _dataFact = (from c in fn.fn_invoices
                                                     where c.AIRLINE_CODE == "999" && c.ID_STATUS == 3
                                                         select new DatosPeticionVivaaerobues() { Pais = c.REQCOUNTRY,  NombreApellido = c.PASSENGER, ReservationDate = c.RESERVATION_DATE, Rfc = c.REQRFC, RazonSocial = c.REQISSUE_NAME, Calle = c.REQSTREET, NExterior = c.REQEXTERNAL_NO, NumeroI = c.REQINTERNAL_NO, Colonia = c.REQCOLONIA, Delegacion = c.REQMUNICIPIO, Estado = c.REQESTADO, CodigoP = c.REQZIPCODE, Pnr = c.ETICKET_PNR, Ciudad = c.REQCOUNTRY }).ToList();

            return _dataFact;
        }

        /// <summary>
        ///Metodo usado para seleccionar el estado del listado de la pagina de Aeromexico comparando el estado que esta en BD con las opciones que nos da la pagina 
        /// </summary>
        /// <param name="estado"></param>
        /// <returns></returns>
        public string estado(string estado)
        {

            string sinAcento = RemoveDiacritics(estado);
            StreamReader srLO = new StreamReader(@"C:\PrubaAM_FROM_STORED_P\estados.csv", true);
                        string lectura;
            string Edo="0";
            while ((lectura = srLO.ReadLine()) != null && Edo == "0")
            {


                var _datos = lectura.Split(',');
                if (sinAcento.ToUpper() == _datos[0])
                {
                    Edo = _datos[0];
                }

            }

            if(Edo == "0")
            {
                if (sinAcento == "DF" || sinAcento.Contains("D.F") || sinAcento.Contains("DISITRITO") || sinAcento.Contains("DISTRITO") || sinAcento.Contains("CIUDAD") || sinAcento.Contains("CD") || sinAcento.Contains("FEDERAL"))
                    Edo = "DISTRITO FEDERAL";
                else
                    Edo = "ESTADO DE MEXICO";
            }
            return Edo;
        }

        string id_emp = "";
        /// <summary>
        /// Metodo para desconprimir los zips descargado de las facturas (este metodo lo utilizan las diferentes aerolineas)
        /// </summary>
        /// <param name="download"></param>
        /// <returns></returns>
        public bool DescomprimirZIP(string download)
        {

            
            try
            {
                
                Stream archivoXML = null;
                Stream archivoPDF = null;

                
                
                DirectoryInfo di = new DirectoryInfo(download);
                
                
                int xmlProcesados = 0;
                foreach (var fi in di.GetFiles())
                {



                    string data = download + "\\" + fi.Name;
                    name = fi.Name;
                    zipProcesado = data;
                    string nombreArchivo = string.Empty;

                   
                    if(data.Contains(".zip"))
                    {
                        var archivo = System.IO.Compression.ZipFile.OpenRead(data);


                        foreach (ZipArchiveEntry entry in archivo.Entries)
                        {

                            if (entry.FullName.EndsWith(".xml", StringComparison.OrdinalIgnoreCase))
                            {
                                MemoryStream ms = new MemoryStream();
                                byte[] buffer = new byte[1024];
                                int read = buffer.Length;
                                archivoXML = entry.Open(); // .Open will return a stream

                                while ((read = archivoXML.Read(buffer, 0, buffer.Length)) > 0)
                                {

                                    ms.Write(buffer, 0, read);
                                }
                                cfdi.XML = null;

                                cfdi.XML = ms.ToArray();
                                xmlProcesados++;



                                int oneTime = 0;

                                foreach (ZipArchiveEntry entry2 in archivo.Entries)
                                {





                                    if (oneTime == 0 && ((entry2.FullName.StartsWith(entry.FullName.Substring(0, entry.FullName.Length - 4), StringComparison.OrdinalIgnoreCase)) || (entry2.FullName.StartsWith("Factura"))) && entry2.FullName.EndsWith(".pdf", StringComparison.OrdinalIgnoreCase))
                                    {

                                        archivoPDF = entry2.Open();

                                        oneTime++;

                                        MemoryStream mpdf = new MemoryStream();
                                        byte[] buffer2 = new byte[16 * 1024];
                                        int read2 = buffer.Length;




                                        while ((read2 = archivoPDF.Read(buffer2, 0, buffer2.Length)) > 0)
                                        {
                                            mpdf.Write(buffer2, 0, read2);
                                        }

                                        cfdi.PDF = mpdf.ToArray();
                                        cfdi.CFD_VERSION = null;
                                        cfdi.TOTAL = 0;
                                        cfdi.RECEIVER_RFC = null;
                                        cfdi.ISSUER_RFC = null;
                                        cfdi.CFDI_UUID = null;
                                        cfdi.FOLIO = null;
                                        try
                                        {
                                            id_emp = lecturaXML(entry.Open(), cfdi.XML, cfdi.PDF);
                                        }
                                        catch (Exception e)
                                        {
                                            if(download.StartsWith("C:\\PrubaAM_FROM_STORED_P") == true)
                                            {
                                                string logProcces =@"C:\PrubaAM_FROM_STORED_P\LogProcesados\";

                                                if (Directory.Exists(logProcces) != true)
                                                {
                                                    // Try to create the directory.
                                                    DirectoryInfo dir = Directory.CreateDirectory(logProcces);
                                                }

                                                StreamWriter swProcesadosError = File.AppendText(logProcces + "ProcesadosError" + DateTime.Now.ToString("yyyyMMdd") + ".txt");

                                                swProcesadosError.WriteLine(DateTime.Now.ToString() + " Error en la lectura de Xml " + entry.Name + " Error: " + e);
                                                swProcesadosError.Close();
                                            }

                                            else
                                            {
                                                string logProcces = download.Substring(0, download.Length - 7) + "\\LogProcesados\\";

                                                if (Directory.Exists(logProcces) != true)
                                                {
                                                    // Try to create the directory.
                                                    DirectoryInfo dir = Directory.CreateDirectory(logProcces);
                                                }

                                                StreamWriter swProcesadosError = File.AppendText(logProcces + "ProcesadosError" + DateTime.Now.ToString("yyyyMMdd") + ".txt");

                                                swProcesadosError.WriteLine(DateTime.Now.ToString() + " Error en la lectura de Xml " + entry.Name + " Error: " + e);
                                                swProcesadosError.Close();
                                            }
                                            
                                        }


                                    }
                                }
                            }



                        }
                        archivoXML.Close();
                        archivoPDF.Close();
                        archivo.Dispose();
                    }





                    if (download.StartsWith("C:\\PrubaAM_FROM_STORED_P") == true && id_emp.Contains("No se pudo consultar el ticket") !=true)
                    {
                        StreamWriter swProcesados = File.AppendText(download + "\\LogProcesados\\Procesados" + DateTime.Now.ToString("yyyyMMdd") + ".txt");
                        swProcesados.WriteLine(DateTime.Now.ToString() + " Se procesaron " + xmlProcesados + " Ticket(s) de la agencia " + id_emp + ", El numero de Xml y Pdf procesados es: " + (xmlProcesados * 2) + " del zip " + zipProcesado);
                        swProcesados.Close();
                    }

                    else if (download.StartsWith("C:\\PrubaAM_FROM_STORED_P") == false && id_emp.Contains("No se pudo consultar el ticket") != true)
                    {
                        string ProcessdownloadPath = download.Substring(0, download.Length - 7) + "\\LogProcesados\\";

                        if (Directory.Exists(ProcessdownloadPath) != true)
                        {
                            // Try to create the directory.
                            DirectoryInfo dir = Directory.CreateDirectory(ProcessdownloadPath);
                        }

                        StreamWriter swProcesados = File.AppendText(ProcessdownloadPath + "Procesados" + DateTime.Now.ToString("yyyyMMdd") + ".txt");
                        swProcesados.WriteLine(DateTime.Now.ToString() + " Se procesaron " + xmlProcesados + " Ticket(s) de la agencia " + id_emp + ", El numero de Xml y Pdf procesados es: " + (xmlProcesados * 2) + " del zip " + zipProcesado);
                        swProcesados.Close();
                    }
                    try
                    {
                   
                    


                        if (download.StartsWith("C:\\PrubaAM_FROM_STORED_P") == true && zipProcesado.Contains(".csv") == false && id_emp.Contains("No se pudo consultar el ticket") !=true)
                        {
                            string downProcessAM = download + "\\procesados\\";
                            if (Directory.Exists(downProcessAM) != true)
                            {
                                // Try to create the directory.
                                DirectoryInfo dir = Directory.CreateDirectory(downProcessAM);
                            }
                            System.IO.File.Move(zipProcesado, downProcessAM + name);
                        }
                        else if (download.StartsWith("C:\\PrubaAM_FROM_STORED_P") == false && zipProcesado.Contains(".csv") == false && id_emp.Contains("No se pudo consultar el ticket") != true)
                        {
                            string Procesados = download.Substring(0, download.Length - 7) + "\\procesados\\";



                            if (Directory.Exists(Procesados) != true)
                            {
                                // Try to create the directory. solo si es AM
                                DirectoryInfo dir = Directory.CreateDirectory(Procesados);
                            }
                            System.IO.File.Move(zipProcesado, Procesados + name);
                        }
                    }
                    catch (Exception e)
                    {
                        if (cfdi.FOLIO.StartsWith("027") == true || cfdi.FOLIO.StartsWith("837") == true || cfdi.FOLIO.StartsWith("014") == true)
                        {
                            string downProcess = download.Substring(0, download.Length - 7) +  "\\LogProcesados\\";

                            if (Directory.Exists(downProcess) != true)
                            {
                                // Try to create the directory.
                                DirectoryInfo dir = Directory.CreateDirectory(downProcess);
                            }
                            StreamWriter swProcesadosErrorAlaska = File.AppendText(downProcess + "ProcesadosError" + DateTime.Now.ToString("yyyyMMdd") + ".txt");

                            swProcesadosErrorAlaska.WriteLine(DateTime.Now.ToString() + " Error al mover zip" + name + " Error: " + e.Message);
                            swProcesadosErrorAlaska.Close();
                            string error = e.Message;
                            if (error.Contains("No se puede crear un archivo que ya existe"))
                            {
                                if (Directory.Exists(download.Substring(0, download.Length - 7) + "\\procesados") != true)
                                {
                                    // Try to create the directory.
                                    DirectoryInfo dir = Directory.CreateDirectory(download.Substring(0, download.Length - 7) + "\\procesados");
                                }
                                System.IO.File.Move(zipProcesado, download.Substring(0, download.Length - 7) + "\\procesados\\date" + DateTime.Now.ToString("yyyyMMddHHmmss") + name);
                            }
                        }
                        else if (cfdi.FOLIO.StartsWith("139") == true)
                        {
                            if (Directory.Exists(@"C:\PrubaAM_FROM_STORED_P\LogProcesados") != true)
                            {
                                // Try to create the directory.
                                DirectoryInfo dir = Directory.CreateDirectory(@"C:\PrubaAM_FROM_STORED_P\LogProcesados");
                            }
                            StreamWriter swProcesadosError = File.AppendText(@"C:\PrubaAM_FROM_STORED_P\LogProcesados\ProcesadosError" + DateTime.Now.ToString("yyyyMMdd") + ".txt");

                            swProcesadosError.WriteLine(DateTime.Now.ToString() + " Error al mover zip" + name + " Error: " + e.Message);
                            swProcesadosError.Close();
                            string error = e.Message;
                            if (error.Contains("No se puede crear un archivo que ya existe"))
                            {
                                if (Directory.Exists(@"C:\PrubaAM_FROM_STORED_P\procesados") != true)
                                {
                                    // Try to create the directory.
                                    DirectoryInfo dir = Directory.CreateDirectory(@"C:\PrubaAM_FROM_STORED_P\procesados");
                                }
                                System.IO.File.Move(zipProcesado, download + "\\procesados\\date" + DateTime.Now.ToString("yyyyMMddHHmmss") + name);
                            }
                        }

                  
                    
                    }
                }
                
            }
            catch (Exception e)
            {   
                if(e.Message.Contains("No se encuentra el registro de fin de directorio central"))
                {
                    if (Directory.Exists(download + "\\ErrorDelZip") != true)
                    {
                        // Try to create the directory.
                        DirectoryInfo dir = Directory.CreateDirectory(download + "\\ErrorDelZip");
                    }
                    System.IO.File.Move(zipProcesado, download + "\\ErrorDelZip\\date" + DateTime.Now.ToString("yyyyMMddHHmmss") + name);

                    if (Directory.Exists(download + "\\ErrorDelZip\\ProcesadosError") != true)
                    {
                        // Try to create the directory.
                        DirectoryInfo dir = Directory.CreateDirectory(download + "\\ErrorDelZip\\ProcesadosError");
                    }
                    StreamWriter swProcesadosError = File.AppendText(download + "\\ErrorDelZip\\ProcesadosError\\" + DateTime.Now.ToString("yyyyMMdd") + ".txt");

                    swProcesadosError.WriteLine(DateTime.Now.ToString() + " Error al mover zip" + name + " Error: el zip regresado por Aeromexico esta dañado");
                    swProcesadosError.Close();
                    int status = 6;

                    cambioStatus(status, FacturacionIVA.cleanPNR);
                }
                
                
            }

            return true;
        }

        

        /// <summary>
        /// Metodo utilizado en Alaska e interjet para comprimir el xml y pdf descargados, debido a que ya se contaba con todo el proceso de descomprimir
        /// se reutilizo, por lo cual era mas sencillo comprimir lo descargado y luego llamar al metodo a hacer un proceso distinto...
        /// </summary>
        /// <param name="download"></param>
        /// <returns></returns>
       public string xml_pdf_to_zip (string download)
       {
          
           string pathZip = download.Substring(0,download.Length-7) + "xml_pdf_zip";

           if (Directory.Exists(pathZip) != true)
           {
               // Try to create the directory.
               DirectoryInfo dir = Directory.CreateDirectory(pathZip);
           }
           string zip = pathZip + "\\" + DateTime.Now.ToString("MMddyyyyss") + ".zip";
          System.IO.Compression.ZipFile.CreateFromDirectory(download + "\\", zip);

          DirectoryInfo di = new DirectoryInfo(download);
           foreach(var item in di.GetFiles())
           {
               System.IO.File.Delete(download + "\\" + item);
           }

           return pathZip;
       }
           

        /// <summary>
        /// Metodo que realizar la extraccion de informacion del xml descargado por las diferentes aerolineas
        /// </summary>
        /// <param name="XML"></param>
        /// <param name="xml"></param>
        /// <param name="pdf"></param>
        /// <returns></returns>
        public string lecturaXML (Stream XML, Binary xml, Binary pdf)
        {
            try
            {
                StreamReader srXML = new StreamReader(XML);
                string lecturaXML = srXML.ReadLine();

                                 

                int ubicacion = 0;
                int limite = 0;
                while (lecturaXML != null)
                {
                    if (cfdi.CFD_VERSION == null || cfdi.CFD_VERSION == "")
                    {
                        ubicacion = lecturaXML.IndexOf("as\" version=");
                        if (ubicacion == -1)
                        {
                            limite = lecturaXML.IndexOf("noCertificado");
                            if (limite != -1)
                            {
                                cfdi.CFD_VERSION = "CFD_V" + lecturaXML.Substring(limite - 5, 3).Substring(0, 1);
                                cfdi.CFD_VERSION = cfdi.CFD_VERSION + lecturaXML.Substring(limite - 5, 3).Substring(2, 1);
                            }

                        }

                        else if (ubicacion != -1)
                        {

                            limite = 3;
                            cfdi.CFD_VERSION = "CFD_V3.2";
                        }
                    }

                    if (cfdi.FOLIO == null || cfdi.FOLIO == "")
                    {
                        ubicacion = lecturaXML.IndexOf("folio=");
                        if (ubicacion != -1)
                        {
                            cfdi.FOLIO = lecturaXML.Substring(ubicacion + 7, 13);
                            if(cfdi.FOLIO.StartsWith("2") == true)
                            {
                                cfdi.FOLIO = cfdi.FOLIO.Substring(0, 12).Insert(0, "0");
                                cfdi.PNR = cfdi.FOLIO.Substring(3, 10);
                            }
                            else if(cfdi.FOLIO.StartsWith("139"))
                                cfdi.PNR = cfdi.FOLIO.Substring(3, 10);
                            else
                            {
                                //
                                var pnrInterjet = cfdi.FOLIO.Split('\"');
                                cfdi.FOLIO = cfdi.PNR = pnrInterjet[0];
                                cfdi.CFD_VERSION = "CFD_V3.2";
                                if(cfdi.FOLIO.StartsWith("14") == true)
                                {
                                    cfdi.FOLIO = cfdi.FOLIO.Insert(0, "0");
                                    cfdi.PNR = cfdi.FOLIO.Substring(3, 10);
                                }
                            }
                                

                        }
                    }

                    if(cfdi.PNR == cfdi.FOLIO)
                    {
                            ubicacion = lecturaXML.IndexOf("noIdentificacion");
                            if(ubicacion != -1)
                            {
                                cfdi.PNR = lecturaXML.Substring(ubicacion + 18,6);
                            }
                            else 
                            {
                                ubicacion = lecturaXML.IndexOf("PNR: ");
                                if(ubicacion !=-1)
                                {
                                    cfdi.PNR = lecturaXML.Substring(ubicacion + 5,6);
                                }
                            }
                                
                    }
                        

                    if (cfdi.CFDI_UUID == null || cfdi.CFDI_UUID == "")
                    {

                        ubicacion = lecturaXML.IndexOf("UUID=");
                        limite = 36;
                        if (ubicacion != -1 && limite != -1)
                            cfdi.CFDI_UUID = lecturaXML.Substring(ubicacion + 6, limite);
                    }

                    if (cfdi.ISSUER_RFC == null || cfdi.ISSUER_RFC == "")
                    {
                        ubicacion = lecturaXML.IndexOf("Emisor rfc=");
                        if (ubicacion != -1)
                        {
                            limite = lecturaXML.IndexOf("nombre");
                            cfdi.ISSUER_RFC = lecturaXML.Substring(ubicacion + 12, limite - ubicacion - 14);
                        }

                    }

                    if (cfdi.RECEIVER_RFC == null || cfdi.RECEIVER_RFC == "")
                    {
                        ubicacion = lecturaXML.IndexOf("Receptor rfc=");
                        if (ubicacion != -1)
                        {
                            //corregir lectura
                            limite = 15;
                            var totrfc = lecturaXML.Substring(ubicacion + 14, 15).Split(' ');
                            cfdi.RECEIVER_RFC = totrfc[0].Substring(0, totrfc[0].Length - 1);
                        }
                    }

                    if (cfdi.TOTAL == 0)
                    {
                        ubicacion = lecturaXML.IndexOf("total");
                        if (ubicacion != -1)
                        {
                            limite = 7;
                            var tot = lecturaXML.Substring(ubicacion + 7, 11).Split(' ');

                            cfdi.TOTAL = Convert.ToDecimal(tot[0].Substring(0, tot[0].Length - 1));
                        }
                    }


                    if(cfdi.PNR != null && cfdi.FOLIO.Contains("942") == true)
                    {
                        ubicacion = lecturaXML.IndexOf("Tarifa del boleto");
                        if(ubicacion != -1)
                        {
                            cfdi.PNR = lecturaXML.Substring(ubicacion + 21, 10);
                        }
                    }




                    lecturaXML = srXML.ReadLine();
                    
                }
                return consultaTicket(cfdi.PNR, xml, pdf, cfdi.CFDI_UUID, cfdi.ISSUER_RFC, cfdi.RECEIVER_RFC, cfdi.TOTAL, cfdi.FOLIO, cfdi.CFD_VERSION);
            }
            catch (Exception ex)
            {
                if(cfdi.FOLIO.StartsWith("139") == true)
                {
                    if (Directory.Exists(@"C:\PrubaAM_FROM_STORED_P\LogProcesados") != true)
                    {
                        // Try to create the directory.
                        DirectoryInfo dir = Directory.CreateDirectory(@"C:\PrubaAM_FROM_STORED_P\LogProcesados");
                    }
                    StreamWriter swProcesadosError = File.AppendText(@"C:\PrubaAM_FROM_STORED_P\LogProcesados\ProcesadosError" + DateTime.Now.ToString("yyyyMMdd") + ".txt");

                    swProcesadosError.WriteLine("Falla en la lectura del XML en el zip: " + zipProcesado);
                    swProcesadosError.Close();
                    string parhError = @"C:\PrubaAM_FROM_STORED_P\ErrorXML";

                    if (Directory.Exists(parhError) != true)
                    {
                        // Try to create the directory.
                        DirectoryInfo dir = Directory.CreateDirectory(parhError);
                    }
                    System.IO.File.Move(zipProcesado, @"C:\PrubaAM_FROM_STORED_P\ErrorXML\Error" + name + DateTime.Now.ToString("ss"));
                    return ex.Message;
                    throw new Exception(ex.Message + "Falla en la lectura del XML en el zip: " + zipProcesado);
                }
                else if(cfdi.FOLIO.StartsWith("027") == true)
                {
                    string parhError = "C:\\Alaska\\LogProcesados";

                    if (Directory.Exists(parhError) != true)
                    {
                        // Try to create the directory.
                        DirectoryInfo dir = Directory.CreateDirectory(parhError);
                    }
                    StreamWriter swProcesadosErrorAlaska = File.AppendText("C:\\Alaska\\LogProcesados\\ProcesadosError" + DateTime.Now.ToString("yyyyMMdd") + ".txt");

                    if (Directory.Exists("C:\\Alaska\\ErrorXML") != true)
                    {
                        // Try to create the directory.
                        DirectoryInfo dir = Directory.CreateDirectory("C:\\Alaska\\ErrorXML");
                    }
                    
                    System.IO.File.Move(zipProcesado, "C:\\Alaska\\ErrorXML\\Error" + name + DateTime.Now.ToString("ss"));
                    swProcesadosErrorAlaska.Close();
                    return ex.Message;
                    throw new Exception(ex.Message + "Falla en la lectura del XML en el zip: " + zipProcesado);
                }
                else if (cfdi.FOLIO.StartsWith("027") == false && cfdi.FOLIO.StartsWith("139") == false)
                {
                    string parhError = "C:\\Interjet\\LogProcesados";

                    if (Directory.Exists(parhError) != true)
                    {
                        // Try to create the directory.
                        DirectoryInfo dir = Directory.CreateDirectory(parhError);
                    }
                    StreamWriter swProcesadosErrorAlaska = File.AppendText("C:\\Interjet\\LogProcesados\\ProcesadosError" + DateTime.Now.ToString("yyyyMMdd") + ".txt");


                    if (Directory.Exists("C:\\Interjet\\ErrorXML") != true)
                    {
                        // Try to create the directory.
                        DirectoryInfo dir = Directory.CreateDirectory("C:\\Interjet\\ErrorXML");
                    }

                    System.IO.File.Move(zipProcesado, "C:\\Interjet\\ErrorXML\\Error" + name + DateTime.Now.ToString("ss"));
                    swProcesadosErrorAlaska.Close();
                    return ex.Message;
                    throw new Exception(ex.Message + "Falla en la lectura del XML en el zip: " + zipProcesado);
                }
                
                
                
            }
            return "";
            
            
        }





        /// <summary>
        /// Metodo para buscar en la tabla fn_invoice datos requeridos para posteriormente realizar una insercion en la tabla fn_invoice_cfdi y actualizar el status a 4
        /// </summary>
        /// <param name="ticket"></param>
        /// <param name="xml"></param>
        /// <param name="pdf"></param>
        /// <param name="cfdi_uuid"></param>
        /// <param name="issuer_rfc"></param>
        /// <param name="receiver_rfc"></param>
        /// <param name="total"></param>
        /// <param name="folio"></param>
        /// <param name="cfd_version"></param>
        /// <returns></returns>
        public string consultaTicket(string ticket, Binary xml, Binary pdf, string cfdi_uuid, string issuer_rfc, string receiver_rfc, 
            decimal total, string folio, string cfd_version)
        {
            fn_invoiceAMDataContext fn = new fn_invoiceAMDataContext();
            string id_e = "";
            try
            {
                var updateId_Status = (from c in fn.fn_invoices where c.ETICKET_PNR.Contains(ticket) select c).FirstOrDefault();
                updateId_Status.ID_STATUS = 4;
                int? id_rfc = updateId_Status.ID_RFC;
                updateId_Status.INVOICE_DATE = DateTime.Now;
                int id_C = updateId_Status.ID_CLIENT;
                id_e = updateId_Status.Id_Emp.ToString();
                int id_invoice = updateId_Status.ID_INVOICE;
                string rfc_ = updateId_Status.REQRFC;
                string airline = updateId_Status.AIRLINE_CODE;

                //llamar a metodo client_id
                string client_i = "";
                if (id_rfc == null)
                    client_i = client_id(id_C);

                else if (id_rfc != null)
                    client_i = id_RFC(id_rfc);

                Insert_fn_invoice_cfdi(xml, pdf, id_invoice, cfdi_uuid, ticket, issuer_rfc, receiver_rfc, total, folio, id_e, client_i, cfd_version, airline);
                fn.SubmitChanges();
            }
            catch (Exception ex )
            {
                if(cfdi.FOLIO.StartsWith("139") == true)
                {
                    StreamWriter swProcesadosError = File.AppendText(@"C:\PrubaAM_FROM_STORED_P\LogProcesados\ProcesadosError" + DateTime.Now.ToString("yyyyMMdd") + ".txt");

                    swProcesadosError.WriteLine("No se encontro el boleto: " + ticket);
                    swProcesadosError.Close();
                    return "No se pudo consultar el ticket: " + ticket;
                    throw new Exception(ex.Message + "No se pudo consultar el ticket: " + ticket);
                }
                else if(cfdi.FOLIO.StartsWith("027") ==true)
                {
                    StreamWriter swProcesadosErrorAlaska = File.AppendText("C:\\Alaska\\XML_PDF\\LogProcesados\\ProcesadosError" + DateTime.Now.ToString("yyyyMMdd") + ".txt");

                    swProcesadosErrorAlaska.WriteLine("No se encontro el boleto: " + ticket);
                    swProcesadosErrorAlaska.Close();
                    return "No se pudo consultar el ticket: " + ticket;
                    throw new Exception(ex.Message + "No se pudo consultar el ticket: " + ticket);
                }
                
                
            }
            
            return id_e;

        }

        public void cambioStatus(int status, string ticket)
        {
            fn_invoiceAMDataContext fn = new fn_invoiceAMDataContext();
           
            try
            {
                string newTicket = ticket.Substring(3, 10);
                var updateId_Status = (from c in fn.fn_invoices where c.ETICKET_PNR.Contains(newTicket) select c).FirstOrDefault();
                updateId_Status.ID_STATUS = status;
                fn.SubmitChanges();
            }
            catch (Exception)
            {
                
                    StreamWriter swProcesadosError = File.AppendText(@"C:\PrubaAM_FROM_STORED_P\LogProcesados\ProcesadosError" + DateTime.Now.ToString("yyyyMMdd") + ".txt");

                    swProcesadosError.WriteLine("No se encontro el boleto: " + ticket);
                    swProcesadosError.Close();
                               


            }

            

        }

        /// <summary>
        /// Metodo para encontrar el id_cliente que posteriormente sera utilizado en la insercion de la tabla fn_invoice_cfdi en caso de ser el id_rfc = null
        /// </summary>
        /// <param name="rfc"></param>
        /// <param name="id_client"></param>
        /// <returns></returns>
        public string client_id(int id_client)
        {
            fn_invoiceAMDataContext fn = new fn_invoiceAMDataContext();
            
            var findClientId = (from c in fn.fn_clients where c.ID_CLIENT == id_client select c).First();

            return findClientId.CLIENT;
        }

        /// <summary>
        /// Metodo para encontrar el id_cliente que posteriormente sera utilizado en la insercion de la tabla fn_invoice_cfdi en caso de ser el id_rfc != null es decir, un rfc alterno
        /// </summary>
        /// <param name="id_rfc"></param>
        /// <returns></returns>
        public string id_RFC(int? id_rfc)
        {
            fn_invoiceAMDataContext fn = new fn_invoiceAMDataContext();
            
                var findbyrfcAlt = (from c in fn.fn_rfcs where c.ID_RFC == id_rfc select c).FirstOrDefault();

                return findbyrfcAlt.CLIENT;
        }


        /// <summary>
        /// Metodo en el cual se realiza la insercion en la tabla fn_invoice_cfdi, una vez que se inserte exitosamente se realiza la actualizacion del status a 4
        /// </summary>
        /// <param name="xml"></param>
        /// <param name="pdf"></param>
        /// <param name="id_invoice"></param>
        /// <param name="cfdi_uuid"></param>
        /// <param name="ticket"></param>
        /// <param name="issuer_rfc"></param>
        /// <param name="receiver_rfc"></param>
        /// <param name="total"></param>
        /// <param name="folio"></param>
        /// <param name="id_e"></param>
        /// <param name="client_i"></param>
        /// <param name="cfd_version"></param>
        /// <param name="airline_code"></param>
        public void Insert_fn_invoice_cfdi(Binary xml, Binary pdf, int id_invoice, string cfdi_uuid, string ticket, string issuer_rfc, string receiver_rfc, decimal total, 
            string folio, string id_e, string client_i, string cfd_version, string airline_code)
        {

            fn_invoiceAMDataContext fn = new fn_invoiceAMDataContext();
            try
            {
                fn.fn_invoice_cfdis.InsertOnSubmit(new fn_invoice_cfdi
                {
                    //Consultar con mario
                    UUID = Guid.NewGuid().ToString("N"),
                    CREATED = DateTime.Now,
                    UPDATED = DateTime.Now,
                    XML = xml,
                    PDF = pdf,

                    ID_INVOICE = id_invoice,
                    CFDI_UUID = cfdi_uuid,
                    PNR = ticket,
                    ISSUER_RFC = issuer_rfc,
                    RECEIVER_RFC = receiver_rfc,
                    TOTAL = total,
                    FOLIO = folio,
                    AIRLINE_CODE = airline_code,
                    AGENCY_CODE = id_e,
                    CLIENT_ID = client_i,
                    VERSION = 0,
                    //falta leer del xml y preguntar a mario el porque aparece como CFD_V32
                    CFD_VERSION = cfd_version

                });
                fn.SubmitChanges();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message + "No se pudo insertar la inf del ticket: " + ticket);
            }
            
        }

        public string estadoAeromar(string estado)
        {
            string sinAcento = RemoveDiacritics(estado);
            StreamReader srLO = new StreamReader(@"C:\AirAeromar\Catalogos\Catalogo_Estados.csv", true);
            string lectura;
            string Edo = "0";
            while ((lectura = srLO.ReadLine()) != null && Edo == "0")
            {


                var _datos = lectura.Split(',');
                if(_datos[1].Contains(sinAcento.ToUpper()))
                {
                    Edo = _datos[0];
                }

            }

            while ((lectura = srLO.ReadLine()) != null && Edo == "DF")
            {


                var _datos = lectura.Split(',');
                if (_datos[1].Contains(sinAcento.ToUpper()))
                {
                    Edo = _datos[0];
                }

            }

            return Edo;
        }

        public string paisAeromar(string pais)
        {
            string sinAcento = RemoveDiacritics(pais);
            StreamReader srLO = new StreamReader(@"C:\AirAeromar\Catalogos\Catalogo_Paises.csv", true);
            string lectura;
            string Pa = "0";
            while ((lectura = srLO.ReadLine()) != null && Pa == "0")
            {
                var _datos = lectura.Split(',');
                
                if (RemoveDiacritics(_datos[1]).Contains(sinAcento.ToUpper()) || RemoveDiacritics(_datos[2]).Contains(sinAcento.ToUpper()))
                {
                    Pa = _datos[0];
                }

            }

            
            return Pa;
        }

        public static string RemoveDiacritics(string text)
        {
            string formD = text.Normalize(NormalizationForm.FormD);
            StringBuilder sb = new StringBuilder();

            foreach (char ch in formD)
            {
                UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(ch);
                if (uc != UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(ch);
                }
            }

            return sb.ToString().Normalize(NormalizationForm.FormC);
        }

    


    }
}