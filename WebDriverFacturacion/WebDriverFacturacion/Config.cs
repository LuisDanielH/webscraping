﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebDriverFacturacion
{
    public class Config
    {
        static Config()
        {
            UpdateAllConfigurations();
        }

        private static string interjetPageToGetInvoice;

        public static string InterjetPageToGetInvoice
        {
            get { return interjetPageToGetInvoice; }
            set { interjetPageToGetInvoice = value; }
        }

        private static string interjetPageToDownloadInvoice;

        public static string InterjetPageToDownloadInvoice
        {
            get { return interjetPageToDownloadInvoice; }
            set { interjetPageToDownloadInvoice = value; }
        }

        private static string alaskaPageToDownloadInvoice;

        public static string AlaskaPageToDownloadInvoice
        {
            get { return alaskaPageToDownloadInvoice; }
            set { alaskaPageToDownloadInvoice = value; }
        }

        private static string vivaAerobusPageToDownloadInvoice;
        public static string VivaAerobusPageToDownloadInvoice
        {
            get { return vivaAerobusPageToDownloadInvoice; }
            set { vivaAerobusPageToDownloadInvoice = value; }
        }

        public static void UpdateAllConfigurations()
        {
            //Pagina para realizar el proceso de facturacion de interjet
            interjetPageToGetInvoice = System.Configuration.ConfigurationManager.AppSettings["GetInvoiceI"];

            //Pagina para descargar facturas y notas existentes (pdf y xml) para interjet
            interjetPageToDownloadInvoice = System.Configuration.ConfigurationManager.AppSettings["DownloadI"];

            //Pagina para descargar facturas (pdf y xml) para alaska
            alaskaPageToDownloadInvoice = System.Configuration.ConfigurationManager.AppSettings["DownloadA"];

            vivaAerobusPageToDownloadInvoice = System.Configuration.ConfigurationManager.AppSettings["DownloadV"];

        }
    }
}
