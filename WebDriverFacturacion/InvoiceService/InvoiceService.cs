﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using WebDriverFacturacion;
using System.IO;
using System.Timers;

namespace InvoiceService
{

    public partial class InvoiceService : ServiceBase
    {
        public System.Timers.Timer tiempo;
        public bool repitUntil;
        public InvoiceService()
        {
            InitializeComponent();
            
            if(!System.Diagnostics.EventLog.SourceExists("MySource"))
            {
                System.Diagnostics.EventLog.CreateEventSource("MySource","MyNewLog");
            }
            eventLog1.Source = "MySource";
            eventLog1.Log = "MyNewLog";
        }

        protected override void OnStart(string[] args)
        {
            if (args.GetLength(0) > 0 && args[0].Equals("DEBUG"))
                System.Diagnostics.Debugger.Launch();

            tiempo = new System.Timers.Timer();
            
            tiempo.Elapsed += new ElapsedEventHandler(tiempo_ejecucion);
            tiempo.Enabled = true;
            
            tiempo.Start();
            tiempo.Interval = 10 * 60 * 1000 * 3;

        }

        protected override void OnStop()
        {

        }

        private void InitializeServiceValues()
        {
            try
            {
                this.CanHandlePowerEvent = true;
                this.CanStop = true;
                this.CanPauseAndContinue = true;
                this.CanShutdown = true;
                this.ServiceName = "Servicio Facturacion AM";
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }



        public void tiempo_ejecucion(object sender, System.Timers.ElapsedEventArgs e)
        {
            int hora = Convert.ToInt32(DateTime.Now.Hour.ToString());
            repitUntil = false;
            if (repitUntil == false && (hora >= 09 && hora < 23))
            {
                repitUntil = true;
                Process myProcess = new Process();
                myProcess.StartInfo.FileName = @"C:\PrubaAM_FROM_STORED_P\WebDriverFacturacion\InvoiceEx\bin\Debug\InvoiceEx.exe";
                myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                myProcess.Start();
                //FacturacionIVA fc = new FacturacionIVA();
                //fc.peticionFacAero();

                repitUntil = false;
            }
        }
    }
}
